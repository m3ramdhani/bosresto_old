 
var App;
if(!window.console) {
    var console = {
        log : function(){},
        warn : function(){},
        error : function(){},
        time : function(){},
        timeEnd : function(){}
    }
}
require.config({
    paths   : {
        "jquery"               : "libs/jquery",
        "jquery-ui"            : "plugins/jquery-ui/jquery-ui.min",
        "bootstrap"            : "../bootstrap/js/bootstrap.min",
        "metisMenu"            : "plugins/metisMenu/metisMenu.min",
        "raphael"              : "plugins/morris/raphael.min",
        "Morris"               : "plugins/morris/morris.min",
        "blockUI"              : "libs/jquery.blockUI.min",
        "datatables"           : "plugins/dataTables/js/jquery.dataTables.min",
        "datatables-bootstrap" : "plugins/dataTables/js/dataTables.bootstrap",
        "chained"              : "libs/jquery.chained",
        "fabric"               : "libs/fabric.require",
        "payment"              : "libs/jquery.payment",
        "timepicker"           : "plugins/timepicker/jquery.timepicker.min",
        "datepair"             : "plugins/timepicker/lib/Datepair",
        "paging"                : "plugins/jquery.simplePagination",
        "filestyle"             : "plugins/bootstrap-filestyle.min",
        "multiselect"           : "plugins/multiselect.min",
        "moment"                : "plugins/moment.min",
        "datetimepicker"       : "plugins/bootstrap-datetimepicker",
        "select2"             : "plugins/select2/select2.min",
        "highcharts"                  : "libs/highcharts",
        "html2canvas"           :"plugins/html2canvas",
        "rgbcolor"           :"plugins/rgbcolor",
        "canvg"           :"plugins/canvg",
        "html2canvassvg"           :"plugins/html2canvas.svg",
    },
    // urlArgs : "bust=" + (new Date()).getTime(),
    waitSeconds: 0,
    shim    : {
        "jquery-ui"            : {
            deps    : ["jquery"],
            exports : "jquery-ui",
            init    : function () {
                console.log('jquery-ui inited..');
            }
        },
        "bootstrap"            : {
            deps    : ["jquery"],
            exports : "bootstrap",
            init    : function () {
                console.log('bootstrap inited..');
            }
        },
        "metisMenu"            : {
            deps    : ["jquery"],
            exports : "metisMenu",
            init    : function () {
                console.log('metisMenu inited..');
            }
        },
        "Morris"               : {
            deps    : ["jquery", "raphael"],
            exports : "Morris",
            init    : function () {
                console.log('morris inited..');
            }
        },
        "datatables-bootstrap" : {
            deps    : ["jquery", "datatables"],
            exports : "datatables-bootstrap",
            init    : function () {
                console.log('datatables-bootstrap inited..');
            }
        },
        "chained"              : {
            deps    : ["jquery"],
            exports : "chained",
            init    : function () {
                console.log('chained inited..');
            }
        },
        "payment"              : {
            deps    : ["jquery"],
            exports : "payment",
            init    : function () {
                console.log('payment inited..');
            }
        },
        "timepicker"           : {
            deps    : ["jquery"],
            exports : "timepicker",
            init    : function () {
                console.log('timepicker inited..');
            }
        },
        "datepair"             : {
            deps    : ["jquery", "timepicker"],
            exports : "datepair",
            init    : function () {
                console.log('datepair inited..');
            }
        },

        "paging"              : {
            deps    : ["jquery"],
            exports : "paging",
            init    : function () {
                console.log('paging inited..');
            }
        },
         "filestyle"              : {
            deps    : ["jquery"],
            exports : "filestyle",
            init    : function () {
                console.log('filestyle inited..');
            }
        },
        "multiselect"              : {
            deps    : ["jquery"],
            exports : "multiselect",
            init    : function () {
                console.log('multiselect inited..');
            }
        },
        "datetimepicker"              : {
            deps    : ["jquery", 'moment'],
            exports : "datetimepicker",
            init    : function () {
                console.log('datetimepicker inited..');
            }
        },
        "select2"              : {
            deps    : ["jquery"],
            exports : "select2",
            init    : function () {
                console.log('select2 inited..');
            }
        },
        "highcharts"              : {
            deps    : ["jquery"],
            exports : "highcharts",
            init    : function () {
                console.log('highcharts inited..');
            }
        },
        "html2canvas"              : {
            deps    : ["jquery"],
            exports : "html2canvas",
            init    : function () {
                console.log('html2canvas inited..');
            }
        },
        "rgbcolor"              : {
            deps    : ["jquery"],
            exports : "rgbcolor",
            init    : function () {
                console.log('rgbcolor inited..');
            }
        },
        "canvg"              : {
            deps    : ["rgbcolor"],
            exports : "canvg",
            init    : function () {
                console.log('canvg inited..');
            }
        },
        "html2canvassvg"              : {
            deps    : ["html2canvas"],
            exports : "html2canvassvg",
            init    : function () {
                console.log('html2canvassvg inited..');
            }
        }
    }
}); 
 
require([ "./main-function","app_minify/app-financial"], function (func,application) {
    App = $.extend(application,func);
    App.init();
});


requirejs.onError = function (err) {
    // window.location.reload(true);
};
