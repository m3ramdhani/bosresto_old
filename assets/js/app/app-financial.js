
define([
    "jquery",
    "jquery-ui",
    "chained",
    "metisMenu",
    "Morris",
    'datatables',
    "bootstrap",
    "datatables-bootstrap",
    "datetimepicker",

], function ($, ui) {
    return {
        baseUrl           : $('#root_base_url').val(),
        adminUrl          : $('#admin_url').val(),
        overlayUI         : $('#cover'),
        financialType        : $('#financial_type').val(),
        pdfTitle: "PDF export",
        exportFileName : "DataTable export",

        init              : function () {
            App.overlayUI.hide();
            App.initFunc(App);
            $('#side-menu').metisMenu();

            $('#month_year').datetimepicker({
                sideBySide: true,
                useCurrent: true,
                format: 'M-YYYY',
            });

            
            if (App.financialType == "profit_loss") {
                App.profitLossUI();
            }else if(App.financialType == "modal"){
                App.financialModalUI();
            }else if(App.financialType == "neraca"){
                App.balancesheetUI();
            }else if(App.financialType == "general_ledger"){
                App.generalLedgerUI();
            }else if(App.financialType == "trial_balance"){
                App.trialBalanceUI();
            }
        }, 
        profitLossUI:function(){
           $("#filter_submit").on('click', function (e) {
            $("#financial_content").html("");
            e.preventDefault();
            
            if($('#store_id').val() == 0){
              App.alert("Please Select Store"); 
              return false; 
            }
             
            console.log($('#start_date').val())
            var request = $.ajax({
                type    : "POST",
                url     : App.adminUrl + "/financial/get_profit_loss",
                data    : {
                  store_id      : $('#store_id').val(),
                  month_year      : $('#month_year').val()
                  
                }
            });

            request.done(function (msg) {
              $("#financial_content").html(msg);
              $('#export_pdf').show();
               $('#export_xls').show();
                $("#export_pdf").unbind('click').bind('click', function (e) {
                  $("#formFilter").attr("action",App.adminUrl + "/financial/export_report_to_pdf");
                  // e.preventDefault();
                  // $.ajax({
                    // url      : App.adminUrl + "/financial/export_report_to_pdf",
                    // type     : "POST",
                    // dataType : "json",
                    // data     : {
                      // type      : 'profit_loss',
                      // store_id      : $('#store_id').val(),
                      // month_year      : $('#month_year').val()

                    // },
                    // success  : function (result) {
                      // console.log(App.baseUrl + result);
                      // if (result != '') {
                              // window.location=App.adminUrl+"download.php?filename="+result;
                              // window.location.href = App.baseUrl + result;
                              // window.open(App.baseUrl + result, '_newtab')
                            // }
                            // else
                              // alert('Export report gagal');
                          // }
                  // });
              });  
            });





           
            }); 
        },
        financialModalUI:function(){
           $("#filter_submit").on('click', function (e) {
            $("#financial_content").html("");
            e.preventDefault();
            
            if($('#store_id').val() == 0){
              App.alert("Please Select Store"); 
              return false; 
            }
             
             
              var request = $.ajax({
                  type    : "POST",
                  url     : App.adminUrl + "/financial/get_financial_modal",
                  data    : {
                    store_id      : $('#store_id').val(),
                    month_year      : $('#month_year').val()
                    
                  }
              });

              request.done(function (msg) {
                $("#financial_content").html(msg);
                $('#export_pdf').show();
                $('#export_xls').show();
                $("#export_pdf").unbind('click').bind('click', function (e) {
                  $("#formFilter").attr("action",App.adminUrl + "/financial/export_report_to_pdf");
                  // e.preventDefault();
                  // $.ajax({
                    // url      : App.adminUrl + "/financial/export_report_to_pdf",
                    // type     : "POST",
                    // dataType : "json",
                    // data     : {
                      // type      : 'modal',
                      // store_id      : $('#store_id').val(),
                      // month_year      : $('#month_year').val()

                    // },
                    // success  : function (result) {
                      // console.log(App.baseUrl + result);
                      // if (result != '') {
                              // // window.location=App.adminUrl+"download.php?filename="+result;
                              // // window.location.href = App.baseUrl + result;
                              // window.open(App.baseUrl + result, '_newtab')
                            // }
                            // else
                              // alert('Export report gagal');
                          // }
                  // });
              });  
            });





           
            }); 
        },
        balancesheetUI:function(){
           $("#filter_submit").on('click', function (e) {
            $("#financial_content").html("");
            e.preventDefault();
            
            if($('#store_id').val() == 0){
              App.alert("Please Select Store"); 
              return false; 
            }
             
             
              var request = $.ajax({
                  type    : "POST",
                  url     : App.adminUrl + "/financial/get_balance_sheet",
                  data    : {
                    store_id      : $('#store_id').val(),
                    month_year      : $('#month_year').val()
                    
                  }
              });

              request.done(function (msg) {
                $("#financial_content").html(msg);
                $('#export_pdf').show();
                $('#export_xls').show();
                $("#export_pdf").unbind('click').bind('click', function (e) {
                  $("#formFilter").attr("action",App.adminUrl + "/financial/export_report_to_pdf");
                  // e.preventDefault();
                  // $.ajax({
                    // url      : App.adminUrl + "/financial/export_report_to_pdf",
                    // type     : "POST",
                    // dataType : "json",
                    // data     : {
                      // type      : 'neraca',
                      // store_id      : $('#store_id').val(),
                      // month_year      : $('#month_year').val()

                    // },
                    // success  : function (result) {
                      // console.log(App.baseUrl + result);
                      // if (result != '') {
                              /* // window.location=App.adminUrl+"download.php?filename="+result;
                              window.location.href = App.baseUrl + result;*/ 
                              // window.open(App.baseUrl + result, '_newtab')
                            // }
                            // else
                              // alert('Export report gagal');
                          // }
                  // });
              });  
            });





           
            }); 
        },
        generalLedgerUI:function(){
          $('#acc_month_year,#acc_month_year input').datetimepicker({
            sideBySide: true,
            useCurrent: true,
            format: 'M-YYYY',
          });
          $("#filter_submit").on('click', function (e) {
            $("#financial_content").html("");
            e.preventDefault();
            if($('#store_id').val() == 0){
              App.alert("Please Select Store"); 
              return false; 
            }
            var request = $.ajax({
              type    : "POST",
              url     : App.adminUrl + "/financial/get_general_ledger",
              data    : {
                store_id   : $('#store_id').val(),
                month_year : $('#acc_month_year input').val()
              }
            });
            request.done(function (msg) {
              $("#financial_content").html(msg);
              $('#export_pdf').show();
              $('#export_xls').show();
              $("#export_pdf").unbind('click').bind('click', function (e) {
                $("#formFilter").attr("action",App.adminUrl + "/financial/export_report_to_pdf");
              });
              $("#export_xls").unbind('click').bind('click', function (e) {
                $("#formFilter").attr("action",App.adminUrl + "/financial/export_report_to_excel");
              }); 
            });
          }); 
        },
        trialBalanceUI:function(){
          $('#acc_month_year,#acc_month_year input').datetimepicker({
            sideBySide: true,
            useCurrent: true,
            format: 'M-YYYY',
          });
          $("#filter_submit").on('click', function (e) {
            $("#financial_content").html("");
            e.preventDefault();
            if($('#store_id').val() == 0){
              App.alert("Please Select Store"); 
              return false; 
            }
            var request = $.ajax({
              type    : "POST",
              url     : App.adminUrl + "/financial/get_trial_balance",
              data    : {
                store_id   : $('#store_id').val(),
                month_year : $('#acc_month_year input').val()
              }
            });
            request.done(function (msg) {
              $("#financial_content").html(msg);
              $('#export_pdf').show();
              $('#export_xls').show();
              $("#export_pdf").unbind('click').bind('click', function (e) {
                $("#formFilter").attr("action",App.adminUrl + "/financial/export_report_to_pdf");
              });
              $("#export_xls").unbind('click').bind('click', function (e) {
                $("#formFilter").attr("action",App.adminUrl + "/financial/export_report_to_excel");
              }); 
            });
          }); 
        },
        uniqueArray       : function (data) {
            var arrayResult = [];

            var arr = data,
                len = arr.length;

            while (len--) {
                var itm = arr[len];
                if (arrayResult.indexOf(itm) === -1) {
                    arrayResult.unshift(itm);
                }
            }

            return arrayResult;
        }
    }
});