define([
    "jquery",
    "jquery-ui",
    "chained",
    "metisMenu",
    "Morris",
    'datatables',
    "bootstrap",
    "select2",
    "datatables-bootstrap",
    "datetimepicker",

], function ($, ui) {
    return {
        overlayUI         : $('#cover'),
        baseUrl                : $('#root_base_url').val(),
        
        init           : function () {
            App.overlayUI.show();
            App.initFunc(App);

            $('.select2').select2();

            $("#bank_id").chained("#store_id");
            $('#side-menu').metisMenu();

            $('#start_date').datetimepicker({
                sideBySide: true,
                useCurrent: true,
                format: 'YYYY-MM-DD',
            });

            $('#end_date').datetimepicker({
                sideBySide: true,
                useCurrent: true,
                format: 'YYYY-MM-DD',
            });


            $("#start_date").on("dp.change", function (e) {

                $('#end_date').datetimepicker({
                    sideBySide: true,
                    useCurrent: true,
                    format: 'YYYY-MM-DD HH:mm' 
                });
                
                $('#end_date').data("DateTimePicker").minDate(e.date);

            });

             $("#end_date").on("dp.change", function (e) {

                $('#end_date').datetimepicker({
                    sideBySide: true,
                    useCurrent: true,
                    format: 'YYYY-MM-DD HH:mm' 
                });
                
                $('#start_date').data("DateTimePicker").maxDate(e.date);

            });

            $("body").tooltip({ selector: '[data-tooltip=tooltip]' });
            $("#province_id_chained").chained("#country_id_chained");
            $("#city_id_chained").chained("#province_id_chained");
var table = $('#dataTables-ar').dataTable({
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerEmployee();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "customer_name"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 5,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });
            $("#filter_submit").on('click', function (e) {
                e.preventDefault();

                 var table = $('#dataTables-ar').dataTable({
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerEmployee();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "customer_name"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 5,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });
            });   
var table = $('#dataTables-ar-company').dataTable({
                    // "scrollX": false,
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerCompany();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "company_name"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 5,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });
            $("#filter_submit_company").on('click', function (e) {
                e.preventDefault();

                 var table = $('#dataTables-ar-company').dataTable({
                    // "scrollX": false,
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerCompany();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "company_name"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 5,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });
            });  
 var table = $('#dataTables-ar-cc').dataTable({
                    "scrollX": false,
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerCC();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "bank_name"},
                        {data : "cc_number"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 6,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });
            $("#filter_submit_cc").on('click', function (e) {
                e.preventDefault();

                 var table = $('#dataTables-ar-cc').dataTable({
                    "scrollX": false,
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerCC();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "bank_name"},
                        {data : "cc_number"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 6,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });
            });   
            var table = $('#dataTables-ar-flazz').dataTable({
                    "scrollX": false,
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerCC();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "bank_name"},
                        {data : "cc_number"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 6,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });       
            $("#filter_submit_flazz").on('click', function (e) {
                e.preventDefault();

                 var table = $('#dataTables-ar-flazz').dataTable({
                    "scrollX": false,
                    "bProcessing"    : true,
                    "bServerSide"    : false,
                    "bFilter"        : false,
                    "bDestroy" :true,
                    "sServerMethod"  : "POST",
                    "ajax": {
                        "url": $('#dataProcessUrl').val(),
                        "type": 'POST',
                        "data": {
                          param: $('#formFilter').serialize()
                        },
                         "dataSrc": function( json ) {
                            App.setListenerCC();
                            return json.data;
                        },                             
                     },
                    "iDisplayLength" : 10,
                    "columns"        : [                    
                        {data : "store_name"},
                        {data : "date"},
                        {data : "receipt_number"},
                        {data : "bank_name"},
                        {data : "cc_number"},
                        {data : "debit"},
                        {data : "actions"}
                        ],
                    "columnDefs"     : [
                        {
                            "targets"     : 6,
                            "orderable"   : false,
                            "bSearchable" : false,
                            "class"       : 'center-tr'
                        }
                    ],
                    "order"          : [[1, "desc"]]
                });
            });
            App.overlayUI.hide();

            $(".datepicker").datepicker({
                dateFormat     : 'yy-mm-dd',
                constrainInput: true,
                showOn: 'button',
                buttonImage: App.baseUrl+'assets/img/calendar_icon.png'   
            });

        },

        setListenerEmployee:function(){          
            $('#dataTables-ar').on('click', '.btn-detail' ,function (e) {
                e.preventDefault();
                $("#name").val($(this).attr('customer_name'));
                $("#member_id").val($(this).attr('customer_id'));
                $("#address").val($(this).attr('customer_address'));
                $("#city").val($(this).attr('customer_city'));
                $("#phone").val($(this).attr('customer_phone'));
                $("#mobile_phone").val($(this).attr('customer_hp'));
                $("#detail-pop-up").modal('show');
            });

            $('#dataTables-ar').on('click', '.btn-payment' ,function (e) {
                e.preventDefault();
                var dataUrl = $(this).attr('data_url');

                function settled_payment() {
                    var request = $.ajax({
                        type: 'POST',
                        url: dataUrl
                    });
                    request.done(function (msg) {
                        var parsedObject = JSON.parse(msg);
                    });
                    request.fail(function (jqXHR, textStatus) {

                    });
                    request.always(function () {
                        location.reload();
                    });
                }

                App.confirm('Lunasi piutang?', settled_payment);
            });
        },

        setListenerCompany:function(){  

            $('#dataTables-ar-company').on('click', '.btn-detail' ,function (e) {
                e.preventDefault();
                $("#name").val($(this).attr('customer_name'));
                $("#pic_name").val($(this).attr('pic_name'));
                $("#address").val($(this).attr('customer_address'));
                $("#email").val($(this).attr('customer_email'));
                $("#phone").val($(this).attr('customer_phone'));
                $("#mobile_phone").val($(this).attr('customer_hp'));
                $("#detail-pop-up").modal('show');
            });

            $('#dataTables-ar-company').on('click', '.btn-payment' ,function (e) {
                e.preventDefault();
                var store_id = $(this).attr('store_id');

                $("#btn_ok_company").attr('store_id',$(this).attr('store_id'));
                $("#btn_ok_company").attr('data_url',$(this).attr('data_url'));
                $("#settled-pop-up").modal('show');
            });


            $('input[type=radio]').on('click',function (e) {
                // e.preventDefault();
                var payment_option = $( "input:checked" ).val();
                if(payment_option==1){
                    $("#data_kartu").hide();
                }else{
                    $("#data_kartu").show();
                }
            });

            $('#btn_ok_company').on('click',function (e) {
                e.preventDefault();
                var dataUrl = $(this).attr('data_url');
                var store_id = $(this).attr('store_id');
                var payment_option = $('input[type=radio]:checked').val();
                var bank = $('#ddl_bank').val();
                var no_kartu = $('#no_kartu').val();

                function settled_payment() {
                    var request = $.ajax({
                        type: 'POST',
                        url: dataUrl,
                        data : {
                            payment_option : payment_option,
                            bank_name : bank,
                            store_id : store_id,
                            no_kartu : no_kartu,
                        },
                    });
                    request.done(function (msg) {
                        var parsedObject = JSON.parse(msg);
                    });
                    request.fail(function (jqXHR, textStatus) {

                    });
                    request.always(function () {
                        location.reload();
                    });
                }
                
                if(payment_option!=1 && ($("#no_kartu").val().length < 16 || $("#no_kartu").val().length > 16)){
                    App.alert("Nomor Kartu Kredit Harus 16 digit");
                }else{
                    App.confirm('Lunasi piutang?', settled_payment);
                }
            });
        },

        setListenerCC:function(){     
            $('#charge').keyup(function (e){
                var total = $('#total').val().replace(/[rp.]/gi, '');
                var charge = 0;
                var income = 0;

                var charCode = (e.which) ? e.which : e.keyCode
                if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57))){
                    var node = $(this);
                    node.val(node.val().replace(/[^0-9.]/g,'') );
                    e.preventDefault();
                }else{
                    console.log(e.key);
                    charge = $('#charge').val();
                    income = parseInt(total) - parseInt(charge);
					if(isNaN(income))income=0;
                    $('#income').val(income);
                }
            });   

            $('#income').keyup(function (e){
                console.log( this.value);
                var total = $('#total').val().replace(/[rp.]/gi, '');
                var charge = 0;
                var income = 0;

                var charCode = (e.which) ? e.which : e.keyCode;
                console.log(charCode);

                if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57))){
                    var node = $(this);
                    node.val(node.val().replace(/[^0-9.]/g,'') );
                    e.preventDefault();
                }else{
                    income = $('#income').val();
                    console.log(total,$('#income').val());
                    charge = parseInt(total) - parseInt(income);
                    $('#charge').val(charge);
                }
            });

            $('#dataTables-ar-cc,#dataTables-ar-flazz').on('click', '.btn-payment' ,function (e) {
                e.preventDefault();
                $('#charge').val('');
                $('#income').val('');
                $('#total').val($(this).attr('total_payment'));
                $('.btn-submit').attr('data_url',$(this).attr('data_url'));
                $('.btn-submit').attr('bank_account_id',$(this).attr('bank_account_id'));
                $("#detail-pop-up").modal('show');
            });

            $('.btn-submit').on('click',function (e) {
                e.preventDefault();
                var dataUrl = $(this).attr('data_url');
                var charge = $('#charge').val();
                var income = $('#income').val();
                var bank_account_id = $(this).attr('bank_account_id');
                if(parseInt(charge) < 0){
                    App.alert("Charge Harus Lebih Besar dari 0");
                    return;
                }

                if(parseInt(income) < 0){
                    App.alert("Income Harus Lebih Besar dari 0");
                    return;
                }
                function settled_payment() {
                    var request = $.ajax({
                        type: 'POST',
                        url: dataUrl,
                        data: {
                            charge: charge,
                            income: income,
                            bank_account_id: bank_account_id,
                        }
                    });
                    request.done(function (msg) {
                        var parsedObject = JSON.parse(msg);
                    });
                    request.fail(function (jqXHR, textStatus) {

                    });
                    request.always(function () {
                        location.reload();
                    });
                }

                App.confirm('Lunasi piutang?', settled_payment);
            });
        }

    };
});