<?php if (! defined('BASEPATH')) exit('No direct script access allowed');


class Financial extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('account_data_model');
        $this->load->model('store_model');
        $this->load->helper(array('datatables'));
        $this->_setting = $this->data['setting'];

        $this->load->model('hrd_model');
        $this->load->model('store_model');
        $this->load->model('supplier_model');
        $this->load->model('user_model');
        $this->load->model('categories_model');
        $this->data['all_store']  = $this->store_model->get_store_dropdown();
        $this->data['all_supplier'] = $this->supplier_model->get_supplier_dropdown();
        $this->data['all_user'] = $this->user_model->get_member_dropdown();
        $this->data['user_store'] = $this->cek_user_store();
		if(is_array($this->data['user_groups_data'])){
			$this->data['user_groups_data'] = $this->data['user_groups_data'][0];
		}
    }

    public function cek_user_store() {
        $user_store = $this->_setting['store_id'];
        if ($user_store <> 0) {
            $status['store_selected'] = $user_store;
            $status['disabled_dropdown'] = 'disabled';
        } else {
            $status['store_selected'] = '';
            $status['disabled_dropdown'] = '';
        }
        return $status;
    }
 
    public function get_profit_loss(){
        $this->data['title']    = "Laporan";
        $this->data['subtitle'] = "Laporan Laba/Rugi";
        $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['message_success'] = $this->session->flashdata('message_success');

        $store_id = $this->input->post('store_id');
        if(!$store_id){
			$store_id = $this->_setting['store_id'];
        }
        $month_year = $this->input->post('month_year');
      
        $this->data['is_print'] = FALSE;
        //pendapatan 
        
        $this->get_data_report($month_year,$store_id);
        $get_profit_loss_income=array();
          $codes=array();
          foreach($this->data['get_profit_loss_income'] as $p){
            if(!in_array($p->code,$codes)){
              array_push($codes,$p->code);
            }
          }
          foreach($codes as $c){
            $jumlah=0;
            $temp=array();
            foreach($this->data['get_profit_loss_income'] as $p){
              if($c==$p->code){
                $temp=$p;
                $jumlah+=$p->jumlah;
              }
            }
            $temp->jumlah=$jumlah;
            array_push($get_profit_loss_income,$temp);
          }
          $this->data['get_profit_loss_income']=$get_profit_loss_income;
        $html = $this->load->view('admin/financial/financial-profit-loss-pdf', $this->data, true);

         echo $html;
    }

    public function profit_loss(){
        $this->data['title']    = "Laporan Keuangan Untung/Rugi";
        $this->data['subtitle'] = "Laporan Keuangan Untung/Rugi";

        $store_id = $this->_setting['store_id'];
        $month_year = date('m-Y');
      
        $this->data['is_print'] = FALSE;
        //pendapatan 
        
        $this->get_data_report($month_year,$store_id);
        $get_profit_loss_income=array();
          $codes=array();
          foreach($this->data['get_profit_loss_income'] as $p){
            if(!in_array($p->code,$codes)){
              array_push($codes,$p->code);
            }
          }
          foreach($codes as $c){
            $jumlah=0;
            $temp=array();
            foreach($this->data['get_profit_loss_income'] as $p){
              if($c==$p->code){
                $temp=$p;
                $jumlah+=$p->jumlah;
              }
            }
            $temp->jumlah=$jumlah;
            array_push($get_profit_loss_income,$temp);
          }
          $this->data['get_profit_loss_income']=$get_profit_loss_income;

        $this->data['content'] .= $this->load->view('admin/financial/financial-profit-loss', $this->data, true);
        
        $this->render('financial');
    }

    public function export_report_to_pdf(){
        $this->load->helper(array('dompdf', 'file'));
        if($this->input->server('REQUEST_METHOD') == 'POST'){
          $store_id = $this->input->post('store_id');
          if(!$store_id){
			$store_id = $this->_setting['store_id'];
          }
          $type = $this->input->post('type');
          $month_year = $this->input->post('month_year');
        }else{
          $store_id = $this->input->get('store_id');
          $type = $this->input->get('type');
          $month_year = $this->input->get('month_year');
        }
		if(!$store_id){
			$store_id = $this->_setting['store_id'];
		}
        $extract_month_year = explode("-", $month_year);
        if(empty($extract_month_year[0])){
          $month = date('m');
          $year = date('Y');
        }else{
          $month = $extract_month_year[0];
          $year = $extract_month_year[1];
        }
		
        $this->get_data_report($month_year,$store_id);
        $pdf_orientation= "landscape";
        $this->data['is_print'] = TRUE;
        if($type == "profit_loss"){
            $pdf_orientation= "portrait";
          $get_profit_loss_income=array();
          $codes=array();
          foreach($this->data['get_profit_loss_income'] as $p){
            if(!in_array($p->code,$codes)){
              array_push($codes,$p->code);
            }
          }
          foreach($codes as $c){
            $jumlah=0;
            $temp=array();
            foreach($this->data['get_profit_loss_income'] as $p){
              if($c==$p->code){
                $temp=$p;
                $jumlah+=$p->jumlah;
              }
            }
            $temp->jumlah=$jumlah;
            array_push($get_profit_loss_income,$temp);
          }
          $this->data['get_profit_loss_income']=$get_profit_loss_income;
            $report = 'Laporan Laba-Rugi';
            $html = $this->load->view('admin/financial/financial-profit-loss-pdf', $this->data, true);    
        }elseif($type == "modal"){
            $month=($month<10 ? "0" : "").(int)$month;
            $where = array(
                "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
                "c.id" => 4,
                "a.store_id"=>$store_id
            );
            $this->data['get_first_modal']    = $this->account_data_model->get_profit_loss_nodebit($where);
            $report = 'Laporan Perubahan Modal';
            $this->data['last_profit_loss']=$this->get_last_month_profit_loss($year,$month,$store_id);
            $html = $this->load->view('admin/financial/financial-modal-pdf', $this->data, true);
        }elseif($type == "neraca"){
            $report = 'Laporan Neraca';
            $this->data['is_print'] = TRUE;
            $extract_month_year = explode("-", $month_year);
            if(empty($extract_month_year[0])){
                $month = date('m');
                $year = date('Y');
            }else{
                $month = $extract_month_year[0];
                $year = $extract_month_year[1];
            }
            $month++;
            if($month>12){
                $month=1;
                $year++;
            }
            $this->data['last_profit_loss']=$this->get_last_month_profit_loss($year,$month,$store_id);
            $this->get_data_report($month_year,$store_id);
            $html = $this->load->view('admin/financial/financial-neraca-pdf', $this->data, true);
        }elseif($type == "general_ledger"){
            $pdf_orientation= "portrait";
            $this->data['year'] =  $year;
            $this->data['month'] =  $this->convert_name_month($month);
            
            $this->data['data_store'] = $this->store_model->get_store($store_id);
            $lists=$this->account_data_model->get_general_ledger(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
            $coa_ledgers=array();
            $results=array();
            $temp=array();
            foreach($lists as $l){
              if(!in_array($l->account_id,$temp)){
                array_push($temp,$l->account_id);
                array_push($coa_ledgers,$l);
              }
              if(!isset($results[$l->account_id]))$results[$l->account_id]=array();
              array_push($results[$l->account_id],$l);
            }
            $this->data['coa_ledgers']=$coa_ledgers;
            $this->data['results']=$results;
            $report = 'Laporan Buku Besar';
            $html = $this->load->view('admin/financial/financial-general-ledger-pdf', $this->data, true);
        }elseif($type=="trial_balance"){
          $pdf_orientation= "portrait";
          $this->data['year'] =  $year;
          $this->data['month'] =  $this->convert_name_month($month);
          $this->data['data_store'] = $this->store_model->get_store($store_id);
          $lists=$this->account_data_model->get_trial_balance(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
          $results=array();
          foreach($lists as $l){
            if((int)$l->DEBIT!=0 || (int)$l->CREDIT!=0){
              array_push($results,$l);
            }
          }
          $this->data['lists']=$results;
          $report = 'Laporan Buku Besar';
          $html = $this->load->view('admin/financial/financial-trial-balance-pdf', $this->data, true);
        }
        $date     = new DateTime();
        $data=pdf_create($html, '', false, $pdf_orientation);
        if(in_array($type,array("trial_balance","general_ledger","modal","profit_loss","neraca"))){
          $filename = 'report_' . $report . '_' . $date->format('Y-m-d') . '_' . $date->format('Gis') . '.pdf';
          header("Content-type:application/pdf");
          header('Content-Disposition: attachment;filename="'.$filename.'"');
          echo $data;
        }else{
          $filename = 'assets/report/report_' . $report . '_' . $date->format('Y-m-d') . '_' . $date->format('Gis') . '.pdf';
          write_file($filename, $data);
          echo json_encode($filename);
        }
    }
    function export_report_to_excel()
    {
      $this->load->library(array('excel'));
      if($this->input->server('REQUEST_METHOD') == 'POST'){
        $store_id = $this->input->post('store_id');
		if(!$store_id){
			$store_id = $this->_setting['store_id'];
		}
        $type = $this->input->post('type');
        $month_year = $this->input->post('month_year');
      }else{
        $store_id = $this->input->get('store_id');
        $type = $this->input->get('type');
        $month_year = $this->input->get('month_year');
      }
      $extract_month_year = explode("-", $month_year);
      if(empty($extract_month_year[0])){
        $month = date('m');
        $year = date('Y');
      }else{
        $month = $extract_month_year[0];
        $year = $extract_month_year[1];
      }
      $store=$this->store_model->get_store($store_id);
      $store=$store[0];
      if($type == "general_ledger"){
        $lists=$this->account_data_model->get_general_ledger(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
        $coa_ledgers=array();
        $results=array();
        $temp=array();
        foreach($lists as $l){
          if(!in_array($l->account_id,$temp)){
            array_push($temp,$l->account_id);
            array_push($coa_ledgers,$l);
          }
          if(!isset($results[$l->account_id]))$results[$l->account_id]=array();
          array_push($results[$l->account_id],$l);
        }
        $style = array(
          "border" => array(
            'borders' => array(
              'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
              )
            )
          ),
          "header_coa"=>array(
            'font' => array(
              'bold' => true
            ),
            "borders"=>array(
              "top"=>array(
                "style"=>PHPExcel_Style_Border::BORDER_NONE
              ),
              "left"=>array(
                "style"=>PHPExcel_Style_Border::BORDER_NONE
              ),
              "right"=>array(
                "style"=>PHPExcel_Style_Border::BORDER_NONE
              ),
              "bottom"=>array(
                "style"=>PHPExcel_Style_Border::BORDER_THIN
              ),
            )
          ),
          "no_border"=>array(
            "allborders"=>array(
              "style"=>PHPExcel_Style_Border::BORDER_NONE
            )
          ),
          "bold"=>array(
            'font' => array(
              'bold' => true
            ),
          ),
          "header_table" => array(
            'alignment' => array(
              'wrap'       => true,
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'font' => array(
              'bold' => true
            ),
            'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'D3E6FC')
            ),
          ),
        );
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Laporan Buku Besar');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Laporan Buku Besar');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $this->excel->getActiveSheet()->setCellValue('A2', 'Cabang : ' . $store->store_name);  
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); 
        $this->excel->getActiveSheet()->mergeCells('A2:E2');
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   

        $this->excel->getActiveSheet()->setCellValue('A3', $this->convert_name_month($month)." ".$year);
        $this->excel->getActiveSheet()->mergeCells('A3:E3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
        
        $row=5;
        $row_table_start=0;
        foreach($coa_ledgers as $c){
          $total_debit=0;
          $total_credit=0;
          $balance=$c->LAST_BALANCE;
          
          $this->excel->getActiveSheet()->setCellValue('A'.$row, $c->name." ( ".$c->code." ) ");
          $this->excel->getActiveSheet()->mergeCells('A'.$row.':E'.$row);
          $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($style['header_coa']);
          $row++;
          $row_table_start=$row;
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,"Tanggal");
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1,$row,"Entry Type");
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2,$row,"Debit");
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3,$row,"Credit");
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4,$row,"Saldo");
          $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($style['header_table']);
          $row++;
          $this->excel->getActiveSheet()->setCellValue('A'.$row, "Saldo Awal");
          $this->excel->getActiveSheet()->mergeCells('A'.$row.':D'.$row);
          $this->excel->getActiveSheet()->setCellValue('E'.$row, $c->LAST_BALANCE)->getStyle('E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          $row++;
          foreach($results[$c->account_id] as $d){
            $total_debit+=$d->debit;
            $total_credit+=$d->credit;
            if($c->debit_multiplier==1){
              $balance+=$d->debit-$d->credit;
            }else{
              $balance+=$d->credit-$d->debit;
            }
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,$d->created_at);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(1,$row,$d->value);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2,$row,$d->debit)->getStyle('C'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3,$row,$d->credit)->getStyle('D'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4,$row,$balance)->getStyle('E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $row++;
          }
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(0,$row,"Sub Total");
          $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(2,$row,$total_debit)->getStyle('C'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(3,$row,$total_credit)->getStyle('D'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          $this->excel->getActiveSheet()->setCellValueByColumnAndRow(4,$row,$balance)->getStyle('E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->applyFromArray($style['bold']);
          $this->excel->getActiveSheet()->getStyle('A'.$row_table_start.':E'.($row))->applyFromArray($style['border']);
          $row+=3;
          $this->excel->getActiveSheet()->getStyle('A'.($row-2).':E'.$row)->applyFromArray($style['no_border']);
        }
        foreach(range('A',$this->excel->getActiveSheet()->getHighestDataColumn()) as $columnID) {
          $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
      }elseif($type=="trial_balance"){
        $lists=$this->account_data_model->get_trial_balance(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
        $results=array();
        foreach($lists as $l){
          if((int)$l->DEBIT!=0 || (int)$l->CREDIT!=0){
            array_push($results,$l);
          }
        }
        $lists=$results;
        $style = array(
          "border" => array(
            'borders' => array(
              'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
              )
            )
          ),
          "center" => array(
            'alignment' => array(
              'wrap'       => true,
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
          ),
          "bold"=>array(
            'font' => array(
              'bold' => true
            ),
          ),
          "header_table" => array(
            'alignment' => array(
              'wrap'       => true,
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'font' => array(
              'bold' => true
            ),
            'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'D3E6FC')
            ),
          ),
        );
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Laporan Neraca Saldo');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Laporan Neraca Saldo');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $this->excel->getActiveSheet()->setCellValue('A2', 'Cabang : ' . $store->store_name);  
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); 
        $this->excel->getActiveSheet()->mergeCells('A2:E2');
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   

        $this->excel->getActiveSheet()->setCellValue('A3', $this->convert_name_month($month)." ".$year);
        $this->excel->getActiveSheet()->mergeCells('A3:E3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
        
        $this->excel->getActiveSheet()->mergeCells('A5:A6');
        $this->excel->getActiveSheet()->setCellValue("A5","Kode Akun");
        $this->excel->getActiveSheet()->mergeCells('B5:B6');
        $this->excel->getActiveSheet()->setCellValue("B5","Nama Akun");
        $this->excel->getActiveSheet()->mergeCells('C5:C6');
        $this->excel->getActiveSheet()->setCellValue("C5","Saldo Awal");
        $this->excel->getActiveSheet()->mergeCells('D5:E5');
        $this->excel->getActiveSheet()->setCellValue("D5","Mutasi Bulan Ini");
        $this->excel->getActiveSheet()->setCellValue("D6","Debit");
        $this->excel->getActiveSheet()->setCellValue("E6","Kredit");
        // $this->excel->getActiveSheet()->mergeCells('F5:F6');
        // $this->excel->getActiveSheet()->setCellValue("F5","Saldo Akhir");
        $this->excel->getActiveSheet()->getStyle('A5:E6')->applyFromArray($style['header_table']);
        $row=7;
        $total_begin_balance=0;
        $total_debit=0;
        $total_credit=0;
        // $total_last_balance=0;
        foreach($lists as $d){
          if($d->LAST_BALANCE==NULL)$d->LAST_BALANCE=0;
          if($d->DEBIT==NULL)$d->DEBIT=0;
          if($d->CREDIT==NULL)$d->CREDIT=0;
          $total_begin_balance+=$d->LAST_BALANCE;
          $total_debit+=$d->DEBIT;
          $total_credit+=$d->CREDIT;
          // if($d->debit_multiplier==1){
            // $total_last_balance+=$d->LAST_BALANCE+$d->DEBIT-$d->CREDIT;
            // $balance=$d->LAST_BALANCE+$d->DEBIT-$d->CREDIT;
          // }else{
            // $total_last_balance+=$d->LAST_BALANCE-$d->DEBIT+$d->CREDIT;
            // $balance=$d->LAST_BALANCE-$d->DEBIT+$d->CREDIT;
          // }
          $this->excel->getActiveSheet()->setCellValue("A".$row,'    '.$d->code);
          $this->excel->getActiveSheet()->setCellValue("B".$row,'    '.$d->name);
          $this->excel->getActiveSheet()->setCellValue("C".$row,$d->LAST_BALANCE)->getStyle('C'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          $this->excel->getActiveSheet()->setCellValue("D".$row,$d->DEBIT)->getStyle('D'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          $this->excel->getActiveSheet()->setCellValue("E".$row,$d->CREDIT)->getStyle('E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          // $this->excel->getActiveSheet()->setCellValue("F".$row,$balance)->getStyle('F'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
          $row++;
        }
        $this->excel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
        $this->excel->getActiveSheet()->setCellValue("A".$row,"Total");
        $this->excel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($style['center']);
        $this->excel->getActiveSheet()->setCellValue("C".$row,$total_begin_balance)->getStyle('C'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $this->excel->getActiveSheet()->setCellValue("D".$row,$total_debit)->getStyle('D'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $this->excel->getActiveSheet()->setCellValue("E".$row,$total_credit)->getStyle('E'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        // $this->excel->getActiveSheet()->setCellValue("F".$row,$total_last_balance)->getStyle('F'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $this->excel->getActiveSheet()->getStyle('A'.$row.':E'.($row))->applyFromArray($style['bold']);
        
        $this->excel->getActiveSheet()->getStyle('A5:E'.($row))->applyFromArray($style['border']);
        foreach(range('A',$this->excel->getActiveSheet()->getHighestDataColumn()) as $columnID) {
          $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
      }
      $date     = new DateTime();
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, "Excel2007");
      if(in_array($type,array("trial_balance","general_ledger")))
      {
        $filename = 'report_' . $type . '_' . $date->format('Y-m-d') . '_' . $date->format('Gis') . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        $objWriter->save('php://output');
      }else{
        $filename = 'assets/report/report_' . $type . '_' . $date->format('Y-m-d') . '_' . $date->format('Gis') . '.xlsx';
        $objWriter->save($filename);
        echo json_encode($filename);        
      }
    }
    function get_data_report($month_year,$store_id){

        $extract_month_year = explode("-", $month_year);

        if(empty($extract_month_year[0])){
            $month = date('m');
            $year = date('Y');
         }else{
            $month = $extract_month_year[0];
            $year = $extract_month_year[1];

        }
        
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 5,
            "a.store_id"=>$store_id
        );
        $this->data['get_profit_loss_income']    = $this->account_data_model->get_profit_loss_nodebit($where);

        //biaya usaha
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 7,
            "a.store_id"=>$store_id
        );
        $this->data['get_profit_loss_incomecost']    = $this->account_data_model->get_profit_loss($where);
         //operasional
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 9,
            "a.store_id"=>$store_id
        );
        $this->data['get_profit_loss_operational']    = $this->account_data_model->get_profit_loss($where);

         //non operasional
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 8,
            "a.store_id"=>$store_id
        );
        $this->data['get_profit_loss_non_operational']    = $this->account_data_model->get_profit_loss($where);


         //pendapatan diluar usaha
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 6,
            "a.store_id"=>$store_id
        );
        $this->data['get_profit_loss_outcome']    = $this->account_data_model->get_profit_loss($where);

        //modal
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 4,
            "a.store_id"=>$store_id
        );
        $this->data['get_first_modal']    = $this->account_data_model->get_profit_loss_nodebit($where);

        //aktiva lancar

        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 1,
            "a.store_id"=>$store_id
        );
        $this->data['get_current_asset']    = $this->account_data_model->get_profit_loss($where);

        //aktiva tetap
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 2,
            "a.store_id"=>$store_id
        );
        $this->data['get_fixed_asset']    = $this->account_data_model->get_profit_loss($where);

        //kewajiban
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 3,
            "a.store_id"=>$store_id
        );
        $this->data['get_liability']    = $this->account_data_model->get_profit_loss($where);

        //prive account
        
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 4,
            "a.store_id"=>$store_id
        );
        $this->data['data_prive']    = $this->account_data_model->get_profit_loss_nocredit($where);
         

        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <=" => $year.$month,
            "c.id" => 3,
            "a.store_id"=>$store_id
        );
        $this->data['get_prive']    = $this->account_data_model->get_profit_loss($where);


        $this->data['data_store'] = $this->store_model->get_store($store_id);    
        $this->data['year'] =  $year;
        $this->data['month'] =  $this->convert_name_month($month);


        $total_pendapatan_usaha = 0;
        foreach ($this->data['get_profit_loss_income'] as $value) {
            $total_pendapatan_usaha += $value->jumlah;
        }
        $total_biaya_usaha = 0;
        foreach ($this->data['get_profit_loss_incomecost'] as $value) {
            $total_biaya_usaha += $value->jumlah;
        }

        $total_laba_kotor = 0;
        $total_laba_kotor = $total_pendapatan_usaha - $total_biaya_usaha;

        $total_biaya_operasional = 0;
        foreach ($this->data['get_profit_loss_operational'] as $value) {
            $total_biaya_operasional += $value->jumlah;
        }

        $total_biaya_non_operasional = 0;
        foreach ($this->data['get_profit_loss_non_operational'] as $value) {
            $total_biaya_non_operasional += $value->jumlah;
        }

        $total_laba_operasional = $total_laba_kotor - $total_biaya_operasional - $total_biaya_non_operasional;


        $total_biaya_diluar = 0;
        foreach ($this->data['get_profit_loss_outcome'] as $value) {
            $total_biaya_diluar += $value->jumlah;
        }

        $laba_bersih = 0;
        $laba_bersih = $total_laba_operasional + $total_biaya_diluar;

        $this->data['total_laba_bersih'] = $laba_bersih;
    }
    function get_last_month_profit_loss($year,$month,$store_id)
    {
      $month=($month<10 ? "0" : "").(int)$month;
      $where = array(
          "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
          "c.id" => 5,
          "a.store_id"=>$store_id
      );
      $get_profit_loss_income    = $this->account_data_model->get_profit_loss_nodebit($where);
      //biaya usaha
      $where = array(
        "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
        "c.id" => 7,
        "a.store_id"=>$store_id
      );
      $get_profit_loss_incomecost    = $this->account_data_model->get_profit_loss($where);
      //operasional
      $where = array(
        "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
        "c.id" => 9,
        "a.store_id"=>$store_id
      );
      $get_profit_loss_operational    = $this->account_data_model->get_profit_loss($where);
      //non operasional
      $where = array(
        "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
        "c.id" => 8,
        "a.store_id"=>$store_id
      );
      $get_profit_loss_non_operational    = $this->account_data_model->get_profit_loss($where);
      //pendapatan diluar usaha
      $where = array(
        "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
        "c.id" => 6,
        "a.store_id"=>$store_id
      );
      $get_profit_loss_outcome    = $this->account_data_model->get_profit_loss($where);
      $total_pendapatan_usaha = 0;
      foreach ($get_profit_loss_income as $value) {
        $total_pendapatan_usaha += $value->jumlah;
      }
      $total_biaya_usaha = 0;
      foreach ($get_profit_loss_incomecost as $value) {
        $total_biaya_usaha += $value->jumlah;
      }
      $total_laba_kotor = 0;
      $total_laba_kotor = $total_pendapatan_usaha - $total_biaya_usaha;
      $total_biaya_operasional = 0;
      foreach ($get_profit_loss_operational as $value) {
        $total_biaya_operasional += $value->jumlah;
      }
      $total_biaya_non_operasional = 0;
      foreach ($get_profit_loss_non_operational as $value) {
        $total_biaya_non_operasional += $value->jumlah;
      }
      $total_laba_operasional = $total_laba_kotor - $total_biaya_operasional - $total_biaya_non_operasional;
      $total_biaya_diluar = 0;
      foreach ($get_profit_loss_outcome as $value) {
        $total_biaya_diluar += $value->jumlah;
      }
      $laba_bersih = $total_laba_operasional + $total_biaya_diluar;
      return $laba_bersih;
    }
    function convert_name_month($index){
         $BulanIndo = array("Januari", "Februari", "Maret",
             "April", "Mei", "Juni",
             "Juli", "Agustus", "September",
             "Oktober", "November", "Desember");
         if($index < 0) return "";
         return $BulanIndo[$index - 1];
    }

    public function get_financial_modal(){
        $this->data['title']    = "Laporan Keuangan Perubahan Modal";
        $this->data['subtitle'] = "Laporan Keuangan Perubahan Modal";
        $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['message_success'] = $this->session->flashdata('message_success');

        $store_id = $this->input->post('store_id');
		if(!$store_id){
			$store_id = $this->_setting['store_id'];
		}
        $month_year = $this->input->post('month_year');
        $extract_month_year = explode("-", $month_year);

        if(empty($extract_month_year[0])){
            $month = date('m');
            $year = date('Y');
         }else{
            $month = $extract_month_year[0];
            $year = $extract_month_year[1];

        }
        $this->data['is_print'] = FALSE;
       
        $this->get_data_report($month_year,$store_id);
        $month=($month<10 ? "0" : "").(int)$month;
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
            "c.id" => 4,
            "a.store_id"=>$store_id
        );
        $this->data['get_first_modal']    = $this->account_data_model->get_profit_loss_nodebit($where);
        $this->data['last_profit_loss']=$this->get_last_month_profit_loss($year,$month,$store_id);
        $html = $this->load->view('admin/financial/financial-modal-pdf', $this->data, true);

         echo $html;
    }

    public function modal(){
        $this->data['title']    = "Laporan Keuangan Laba Berjalan";
        $this->data['subtitle'] = "Laporan Keuangan Laba Berjalan";

        $store_id = $this->_setting['store_id'];
        $month_year = date("m-Y");
        $extract_month_year = explode("-", $month_year);

        if(empty($extract_month_year[0])){
            $month = date('m');
            $year = date('Y');
         }else{
            $month = $extract_month_year[0];
            $year = $extract_month_year[1];

        }
        $this->data['is_print'] = FALSE;
       
        $this->get_data_report($month_year,$store_id);
        $month=($month<10 ? "0" : "").(int)$month;
        $where = array(
            "CONCAT(YEAR(a.created_at),IF(MONTH(a.created_at)<10,'0',''),MONTH(a.created_at)) <" => $year.$month,
            "c.id" => 4,
            "a.store_id"=>$store_id
        );
        $this->data['get_first_modal']    = $this->account_data_model->get_profit_loss_nodebit($where);
        $this->data['last_profit_loss']=$this->get_last_month_profit_loss($year,$month,$store_id);
           
        $this->data['content'] .= $this->load->view('admin/financial/financial-modal', $this->data, true);
        
         $this->render('financial');
    }

    public function neraca(){
        $this->data['title']    = "Laporan Keuangan Neraca";
        $this->data['subtitle'] = "Laporan Keuangan Neraca";

        $store_id = $this->_setting['store_id'];
        $month_year = date("m-Y");
       
        $this->data['is_print'] = FALSE;
        $extract_month_year = explode("-", $month_year);
        if(empty($extract_month_year[0])){
          $month = date('m');
          $year = date('Y');
        }else{
          $month = $extract_month_year[0];
          $year = $extract_month_year[1];
        }
        $month++;
        if($month>12){
          $month=1;
          $year++;
        }
        $this->data['last_profit_loss']=$this->get_last_month_profit_loss($year,$month,$store_id);
        $this->get_data_report($month_year,$store_id);
           
        $this->data['content'] .= $this->load->view('admin/financial/financial-neraca', $this->data, true);
        
         $this->render('financial');
    }

    public function get_balance_sheet(){
        $this->data['title']    = "Laporan Keuangan";
        $this->data['subtitle'] = "Laporan Keuangan";
        $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['message_success'] = $this->session->flashdata('message_success');

        $store_id = $this->input->post('store_id');
		if(!$store_id){
			$store_id = $this->_setting['store_id'];
		}
        $month_year = $this->input->post('month_year');
       
        $this->data['is_print'] = FALSE;
        $extract_month_year = explode("-", $month_year);
        if(empty($extract_month_year[0])){
          $month = date('m');
          $year = date('Y');
        }else{
          $month = $extract_month_year[0];
          $year = $extract_month_year[1];
        }
        $month++;
        if($month>12){
          $month=1;
          $year++;
        }
        $this->data['last_profit_loss']=$this->get_last_month_profit_loss($year,$month,$store_id);
        $this->get_data_report($month_year,$store_id);
        
         $html = $this->load->view('admin/financial/financial-neraca-pdf', $this->data, true);

         echo $html;
    }
    public function general_ledger()
    {
      $this->data['title']    = "Laporan Buku Besar";
      $this->data['subtitle'] = "Laporan Buku Besar";

      $store_id = $this->_setting['store_id'];

      $month_year = date("m-Y");
      $this->data['is_print'] = FALSE;
      $extract_month_year = explode("-", $month_year);
      if(empty($extract_month_year[0])){
        $month = date('m');
        $year = date('Y');
      }else{
        $month = $extract_month_year[0];
        $year = $extract_month_year[1];
      }
      $this->data['year'] =  $year;
      $this->data['month'] =  $this->convert_name_month($month);
      $this->data['data_store'] = $this->store_model->get_store($store_id);
      $lists=$this->account_data_model->get_general_ledger(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
      $coa_ledgers=array();
      $results=array();
      $temp=array();
      foreach($lists as $l){
        if(!in_array($l->account_id,$temp)){
          array_push($temp,$l->account_id);
          array_push($coa_ledgers,$l);
        }
        if(!isset($results[$l->account_id]))$results[$l->account_id]=array();
        array_push($results[$l->account_id],$l);
      }
      $this->data['coa_ledgers']=$coa_ledgers;
      $this->data['results']=$results;

      $this->data['content'] .= $this->load->view('admin/financial/financial-general-ledger', $this->data, true);
      $this->render('financial');
    }
    public function get_general_ledger()
    {
      $this->data['title']    = "Laporan";
      $this->data['subtitle'] = "Laporan Buku Besar";
      $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
      $this->data['message_success'] = $this->session->flashdata('message_success');

      $store_id = $this->input->post('store_id');
		if(!$store_id){
			$store_id = $this->_setting['store_id'];
		}
      $month_year = $this->input->post('month_year');
      $this->data['is_print'] = FALSE;
      $extract_month_year = explode("-", $month_year);
      if(empty($extract_month_year[0])){
        $month = date('m');
        $year = date('Y');
      }else{
        $month = $extract_month_year[0];
        $year = $extract_month_year[1];
      }
      $this->data['year'] =  $year;
      $this->data['month'] =  $this->convert_name_month($month);
      $this->data['data_store'] = $this->store_model->get_store($store_id);
      $lists=$this->account_data_model->get_general_ledger(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
      $coa_ledgers=array();
      $results=array();
      $temp=array();
      foreach($lists as $l){
        if(!in_array($l->account_id,$temp)){
          array_push($temp,$l->account_id);
          array_push($coa_ledgers,$l);
        }
        if(!isset($results[$l->account_id]))$results[$l->account_id]=array();
        array_push($results[$l->account_id],$l);
      }
      
      $this->data['coa_ledgers']=$coa_ledgers;
      $this->data['results']=$results;
      $this->load->view('admin/financial/financial-general-ledger-pdf', $this->data);
    }
    public function trial_balances()
    {
      $this->data['title']    = "Laporan Neraca Saldo";
      $this->data['subtitle'] = "Laporan Neraca Saldo";

      $store_id = $this->_setting['store_id'];
      $month_year = date("m-Y");
      $this->data['is_print'] = FALSE;
      $extract_month_year = explode("-", $month_year);
      if(empty($extract_month_year[0])){
        $month = date('m');
        $year = date('Y');
      }else{
        $month = $extract_month_year[0];
        $year = $extract_month_year[1];
      }
      $this->data['year'] =  $year;
      $this->data['month'] =  $this->convert_name_month($month);
      $this->data['data_store'] = $this->store_model->get_store($store_id);
      $lists=$this->account_data_model->get_trial_balance(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
      $results=array();
      foreach($lists as $l){
        if((int)$l->DEBIT!=0 || (int)$l->CREDIT!=0){
          array_push($results,$l);
        }
      }
      $this->data['lists']=$results;

      $this->data['content'] .= $this->load->view('admin/financial/financial-trial-balance', $this->data, true);
      $this->render('financial');
    }
    public function get_trial_balance()
    {
      $this->data['title']    = "Laporan";
      $this->data['subtitle'] = "Laporan Neraca Saldo";
      $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
      $this->data['message_success'] = $this->session->flashdata('message_success');

      $store_id = $this->input->post('store_id');
		if(!$store_id){
			$store_id = $this->_setting['store_id'];
		}
      $month_year = $this->input->post('month_year');
      $this->data['is_print'] = FALSE;
      $extract_month_year = explode("-", $month_year);
      if(empty($extract_month_year[0])){
        $month = date('m');
        $year = date('Y');
      }else{
        $month = $extract_month_year[0];
        $year = $extract_month_year[1];
      }
      $this->data['year'] =  $year;
      $this->data['month'] =  $this->convert_name_month($month);
      $this->data['data_store'] = $this->store_model->get_store($store_id);
      $lists=$this->account_data_model->get_trial_balance(array("year"=>$year,"month"=>$month,"store_id"=>$store_id));
      $results=array();
      foreach($lists as $l){
        if((int)$l->DEBIT!=0 || (int)$l->CREDIT!=0){
          array_push($results,$l);
        }
      }
      $this->data['lists']=$results;
      $this->load->view('admin/financial/financial-trial-balance-pdf', $this->data);
    }
}