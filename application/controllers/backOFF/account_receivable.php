<?php if (! defined('BASEPATH')) exit('No direct script access allowed');


class account_receivable extends Admin_Controller
{
    private $_setting;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('store_model');
        $this->load->model('account_data_model');
        $this->_setting = $this->data['setting'];
        $this->data['all_store']  = $this->store_model->get_store_dropdown();
        $this->data['user_store'] = $this->cek_user_store();
        
    }

    public function cek_user_store() {
        $user_store = $this->_setting['store_id'];
        if ($user_store <> 0) {
            $status['store_selected'] = $user_store;
            $status['disabled_dropdown'] = 'disabled';
        } else {
            $status['store_selected'] = '';
            $status['disabled_dropdown'] = '';
        }
        return $status;
    }

    public function index(){
        redirect(SITE_ADMIN . '/account_receivable/employee');
    }

    public function employee(){
        $this->load->model('account_data_model');

        $this->data['title']    = "Account Receivable";
        $this->data['subtitle'] = "Employee";

        $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['message_success'] = $this->session->flashdata('message_success');

        $this->data['account_receivable'] = $this->account_data_model->get_account_receivable(array());
        // print_r( $this->data['account_receivable'] );
        // die();

        $this->data['data_url'] = base_url(SITE_ADMIN . '/account_receivable/get_data_employee');;

        //load content
        $this->data['content'] .= $this->load->view('admin/account_receivable/ar-list', $this->data, true);
        
        $this->render('report');
    }

    public function company(){
        $this->load->model('account_data_model');

        $this->data['title']    = "Account Receivable";
        $this->data['subtitle'] = "Company";

        $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['message_success'] = $this->session->flashdata('message_success');

        $this->data['account_receivable'] = $this->account_data_model->get_account_receivable(array());
        // print_r( $this->data['account_receivable'] );
        // die();

        $this->data['all_bank'] = $this->account_data_model->get_bank_distinct(); 
        $this->data['data_url'] = base_url(SITE_ADMIN . '/account_receivable/get_data_company');;

        //load content
        $this->data['content'] .= $this->load->view('admin/account_receivable/ar-list-company', $this->data, true);
        
        $this->render('report');
    }

    public function cc(){
        $this->load->model('account_data_model');

        $this->data['title']    = "Account Receivable";
        $this->data['subtitle'] = "Debit/Credit Card";

        $this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        $this->data['message_success'] = $this->session->flashdata('message_success');

        $this->data['account_receivable'] = $this->account_data_model->get_account_receivable(array());
        // print_r( $this->data['account_receivable'] );
        // die();

        $this->data['all_bank'] = $this->account_data_model->get_bank_store_dropdown(0); 
        // print_r( $this->data['all_bank']);
        // die();
        $this->data['data_url'] = base_url(SITE_ADMIN . '/account_receivable/get_data_cc');;

        //load content
        $this->data['content'] .= $this->load->view('admin/account_receivable/ar-list-cc', $this->data, true);
        
        $this->render('report');
    }
		public function flazz(){
			$this->load->model('account_data_model');
			$this->data['title']    = "Account Receivable";
			$this->data['subtitle'] = "Flazz Card";
			$this->data['message']         = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->data['message_success'] = $this->session->flashdata('message_success');
			$this->data['account_receivable'] = $this->account_data_model->get_account_receivable(array());
			$this->data['all_bank'] = $this->account_data_model->get_bank_store_dropdown(0); 
			$this->data['data_url'] = base_url(SITE_ADMIN . '/account_receivable/get_data_flazz');;
			$this->data['content'] .= $this->load->view('admin/account_receivable/ar-list-flazz', $this->data, true);        
			$this->render('report');
    }
    public function get_data_employee(){
        $post_array = array();
        parse_str($this->input->post('param'), $post_array);

        if ($this->_setting['store_id'] == 0){
        $store_id =$post_array['store_id'];
        } else {
        $store_id =$this->_setting['store_id'];
        }

        $start_date = $post_array['start_date'];
        $end_date = $post_array['end_date'];

        $this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));

        $where_clause = array();
        if($start_date && $end_date){
            $where_clause["date(account_data.created_at) >="] = $start_date;
            $where_clause["date(account_data.created_at) <="] = $end_date;
        }

        if($store_id){
            $where_clause["account_data.store_id"] = $store_id;
        }

        $this->datatables->select('account_data.id, account_data.store_id, store_name, receipt_number, status_ar, 
            entry_type, foreign_id, account_id, 
            debit, credit, date(account_data.created_at) as date, 
            account.code, account.name, payment_option, enum_payment_option.value as payment_option_name , 
            bill_payment.info as customer_id, member.name as customer_name')
        ->from('account_data')
        ->join('bill', 'account_data.foreign_id = bill.id')
        ->join('store', 'account_data.store_id = store.id')
        ->join('account', 'account_data.account_id = account.id')
        ->join('bill_payment', 'bill_payment.bill_id = foreign_id')
        ->join('enum_payment_option', 'enum_payment_option.id = payment_option')
        ->join('member', 'member.id = bill_payment.info')
        ->where('payment_option',7)
        ->where('status_ar <>',2)
        ->where($where_clause)
        ->like('code','103','after')
        ->unset_column('debit')
        ->add_column('debit', '$1', 'convert_rupiah(debit)')
        ->add_column('actions', '$1','generate_action_ar_employee(customer_id,id,status_ar)');
       
        echo $this->datatables->generate();
    }

    public function get_data_company(){
        $post_array = array();
        parse_str($this->input->post('param'), $post_array);

        if ($this->_setting['store_id'] == 0){
        $store_id =$post_array['store_id'];
        } else {
        $store_id =$this->_setting['store_id'];
        }

        $start_date = $post_array['start_date'];
        $end_date = $post_array['end_date'];

        $this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));

        $where_clause = array();
        if($start_date && $end_date){
            $where_clause["date(account_data.created_at) >="] = $start_date;
            $where_clause["date(account_data.created_at) <="] = $end_date;
        }

        if($store_id){
            $where_clause["account_data.store_id"] = $store_id;
        }

        $this->datatables->select('account_data.id, account_data.store_id as current_store, store_name, receipt_number, status_ar, 
            entry_type, foreign_id, account_id, 
            debit, credit, date(account_data.created_at) as date, 
            account.code, account.name, payment_option, enum_payment_option.value as payment_option_name , 
            bill_payment.info as company_id, company_name ')
        ->from('account_data')
        ->join('bill', 'account_data.foreign_id = bill.id')
        ->join('store', 'account_data.store_id = store.id')
        ->join('account', 'account_data.account_id = account.id')
        ->join('bill_payment', 'bill_payment.bill_id = foreign_id')
        ->join('enum_payment_option', 'enum_payment_option.id = payment_option')
        ->join('order_company', 'order_company.id = bill_payment.info')
        ->where('payment_option',6)
        ->where('status_ar <>',2)
        ->where('account_data.account_id',$this->_setting['piutang_dagang_account_id'])
        ->where($where_clause)
        ->like('code','103','after')
        ->unset_column('debit')
        ->add_column('debit', '$1', 'convert_rupiah(debit)')
        ->add_column('actions', '$1','generate_action_ar_company(company_id,id,status_ar,current_store)');
       
        echo $this->datatables->generate();
    }
  
     public function get_data_cc(){
        $post_array = array();
        parse_str($this->input->post('param'), $post_array);

        if ($this->_setting['store_id'] == 0){
        $store_id =$post_array['store_id'];
        } else {
        $store_id =$this->_setting['store_id'];
        }

        $bank_id = FALSE;
        if(isset($post_array['bank_id'])){
            $bank_id = $post_array['bank_id'];
        }

        $start_date = $post_array['start_date'];
        $end_date = $post_array['end_date'];

        $this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));

        $where_clause = array();
        if($start_date && $end_date){
            $where_clause["date(account_data.created_at) >="] = $start_date;
            $where_clause["date(account_data.created_at) <="] = $end_date;
        }

        if($store_id){
            $where_clause["account_data.store_id"] = $store_id;
        }

        if($bank_id){
            $where_clause["account_data.info"] = $bank_id;
        }

        $this->datatables->select('account_data.id, account_data.store_id, store_name, receipt_number, status_ar, 
            entry_type, foreign_id, account_data.account_id, 
            debit, credit, date(account_data.created_at) as date, 
            account.code, account.name, bank_name, bank_account.account_id as bank_account_id, 
            account_data_detail.info as cc_number')
        ->from('account_data')
        ->join('bill', 'account_data.foreign_id = bill.id')
        ->join('store', 'account_data.store_id = store.id')
        ->join('account', 'account_data.account_id = account.id')
        ->join('bill_payment', 'bill_payment.bill_id = foreign_id')
        // ->join('enum_payment_option', 'enum_payment_option.id = payment_option')
        ->join('bank_account', 'account_data.info = bank_account.id')
        ->join('account_data_detail', 'account_data_detail.account_data_id = account_data.id','left')
        ->where('payment_option in (2,3)')
        ->where('status_ar <>',2)
        ->where($where_clause)
        // ->like('code','103','after') 
        // ->or_where("account.code like '%102%'")
        ->unset_column('debit')
        ->add_column('debit', '$1', 'convert_rupiah(debit)')
        ->add_column('actions', '$1','generate_action_ar_cc(id,status_ar,debit,bank_account_id)');
       
        echo $this->datatables->generate();
    }
		public function get_data_flazz(){
        $post_array = array();
        parse_str($this->input->post('param'), $post_array);

        if ($this->_setting['store_id'] == 0){
        $store_id =$post_array['store_id'];
        } else {
        $store_id =$this->_setting['store_id'];
        }

        $bank_id = FALSE;
        if(isset($post_array['bank_id'])){
            $bank_id = $post_array['bank_id'];
        }

        $start_date = $post_array['start_date'];
        $end_date = $post_array['end_date'];

        $this->load->library(array('datatables'));
        $this->load->helper(array('datatables'));

        $where_clause = array();
        if($start_date && $end_date){
            $where_clause["date(account_data.created_at) >="] = $start_date;
            $where_clause["date(account_data.created_at) <="] = $end_date;
        }

        if($store_id){
            $where_clause["account_data.store_id"] = $store_id;
        }

        if($bank_id){
            $where_clause["account_data.info"] = $bank_id;
        }

        $this->datatables->select('account_data.id, account_data.store_id, store_name, receipt_number, status_ar, 
            entry_type, foreign_id, account_data.account_id, 
            debit, credit, date(account_data.created_at) as date, 
            account.code, account.name, bank_name, bank_account.account_id as bank_account_id, 
            account_data_detail.info as cc_number')
        ->from('account_data')
        ->join('bill', 'account_data.foreign_id = bill.id')
        ->join('store', 'account_data.store_id = store.id')
        ->join('account', 'account_data.account_id = account.id')
        ->join('bill_payment', 'bill_payment.bill_id = foreign_id')
        ->join('bank_account', 'account_data.info = bank_account.id')
        ->join('account_data_detail', 'account_data_detail.account_data_id = account_data.id','left')
        ->where('payment_option',11)
        ->where('status_ar <>',2)
        ->where($where_clause)
        ->unset_column('debit')
        ->add_column('debit', '$1', 'convert_rupiah(debit)')
        ->add_column('actions', '$1','generate_action_ar_cc(id,status_ar,debit,bank_account_id)');
       
        echo $this->datatables->generate();
    }
    public function settled($id=0, $type=1){
        $income = $this->input->post('income');
        $charge = $this->input->post('charge');
        $bank_account_id = $this->input->post('bank_account_id');
        $bank_name = $this->input->post('bank_name');
        $store_id = $this->input->post('store_id');
        $payment_option = $this->input->post('payment_option');
        $cc_no = $this->input->post('no_kartu');
        // echo $bank_name . ' | ' . $store_id. ' | ' . $payment_option;
        // die();

        $result = new stdClass();
        $result->status = false;
        $result->message = 'failed';
        $result->redirect = 'failed';
        if($id === 0){

        }else{
            $account_data = $this->account_data_model->get_one($id);
            $date = date('Y-m-d H:i:s', time());
            $account_data_detail = array();
            $debit_account = array(
                'account_id' => $this->_setting['temporary_cash_account_id'],
                'store_id' => $account_data->store_id,
                'entry_type' => 3,
                'foreign_id' => $account_data->foreign_id,
                'info' => 0,
                'debit' => $account_data->debit,
                'credit' => 0,
                'created_at' => $date,
                'status_ar' => 2
            );
            
            $credit_account = array(
                'account_id' => $this->_setting['piutang_karyawan_account_id'],
                'store_id' => $account_data->store_id,
                'entry_type' => 3,
                'foreign_id' => $account_data->foreign_id,
                'info' => 0,
                'debit' => 0,
                'credit' => $account_data->debit,
                'created_at' => $date,
                'status_ar' => 2
            );
            if($type==2){
                switch ($payment_option) {
                    case 1:
                        $debit_account['account_id'] = $this->_setting['temporary_cash_account_id'];
                        break;
                    case 2:
                        $account_data_detail = array(
                            'account_data_id' => 0 ,
                            'info' => $cc_no,
                            'description' => 'no_debit',
                            'store_id' => $account_data->store_id,
                            'created_at' => $date
                        );

                        $bank = $this->account_data_model->get_bank_by_store_and_name($store_id,$bank_name);
                        $debit_account['account_id'] = $bank->account_id;
                        $debit_account['info'] = $bank->id;
                        break;
                    case 3:
                        $account_data_detail = array(
                            'account_data_id' => 0 ,
                            'info' => $cc_no,
                            'description' => 'no_cc',
                            'store_id' => $account_data->store_id,
                            'created_at' => $date
                        );

                        $bank = $this->account_data_model->get_bank_by_store_and_name($store_id,$bank_name);
                        $debit_account['account_id'] = $this->_setting['credit_receivable_account_id'];
                        $debit_account['info'] = $bank->id;
                        $debit_account['status_ar'] = 0;
                        break;
                    default:
                        $debit_account['account_id'] = $this->_setting['temporary_cash_account_id'];
                }
                $credit_account['account_id'] = $this->_setting['piutang_dagang_account_id'];
            }else if($type==3){
                $credit_account['account_id'] = $this->_setting['credit_receivable_account_id'];
                $debit_account['debit'] = $income;
                $debit_account['account_id'] = $bank_account_id;

                $biaya_account = array(
                    'account_id' => $this->_setting['biaya_adm_bank_account'],
                    'store_id' => $account_data->store_id,
                    'entry_type' => 3,
                    'info' => 0,
                    'foreign_id' => $account_data->foreign_id,
                    'debit' => $charge,
                    'credit' => 0,
                    'status_ar' => 2,
                    'created_at' => $date

                );
                $this->account_data_model->add($biaya_account); 
            }

            $new_id = $this->account_data_model->add($debit_account);            
            $this->account_data_model->add($credit_account);

            $account_data_detail['account_data_id'] = $new_id;
            $this->account_data_model->add_detail($account_data_detail);

            $this->account_data_model->update(array('status_ar' => 1), array('id' => $id));

			//update to server	
			if(!empty($account_data->token)){
				$data_to_send = array(
					"condition" => json_encode(array("token" => $account_data->token)),
					"update" => json_encode(array("status_ar" => 1)),
				);
				
				$return_post = $this->update_status($data_to_send);
			}
			
            $result->status = true;
            $result->message = 'Success';
            $this->session->set_flashdata('message_success', 'Data Berhasil diproses.');
        }
        // redirect(SITE_ADMIN . '/account_receivable/employee');
        echo json_encode($result);
    }
	
	function update_status($dataToSend = array()){
		if(empty($dataToSend)) return false;
		$url_compl = $this->data['setting']['server_base_url']."api/account_receivable/update";
		$return = $this->curl_connect($dataToSend, $url_compl);
		return $return;
	}
	
	function curl_connect($data, $url)
    {
        //open connection
        $ch = curl_init();

        curl_setopt_array($ch, array(CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Api Stock Request',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data));

        //execute post
        $result = curl_exec($ch);
        /* Check HTTP Code */
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //close connection
        curl_close($ch);

        /* 200 Response! */

        // var_dump($result);die();
        $result = json_decode($result);

        if ($status == 200) {
            if ($result->status == TRUE) {
                return $result;
            } else {
            }


        } else {
            // curl failed
        }

        return FALSE;
    }

}