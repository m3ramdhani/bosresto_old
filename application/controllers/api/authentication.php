<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Authentication extends REST_Controller {
    public function __construct() {
        parent::__construct();

        $this->load->library('encrypt');
    }

    public function login_post() {
        // Get the post data
        $identity = $this->post('pin');

        // Validate the post data
        if(!empty($identity)){
            $get_users = $this->db->get('users')->result();
            $pins = array();
            foreach ($get_users as $key) {
                $pins[$key->pin] = $this->encrypt->decode($key->pin);
            }
            $pin = array_search($identity, $pins, TRUE);
            
            if($pin){
                $query = $this->db->select('pin, username, email, id, password, active, last_login')
                    ->where('pin', $pin)
                    ->limit(1)->order_by('id', 'desc')->get('users');

                if ($query->num_rows() === 1){
                    $user = $query->row();
                    
                    $api_token = $this->encrypt->encode($user->id);
                    $this->db->where('id', $user->id);
                    $this->db->update('users', array('api_token' => $api_token));

                    // Set the response and exit
                    $this->response([
                        'status' => TRUE,
                        'message' => 'User login successful.',
                        'data' => $user
                    ], 200);
                } else {
                    // Set the response and exit
                    //BAD_REQUEST (400) being the HTTP response code
                    $this->response("Wrong PIN.", 400);
                }
            } else {
                // Set the response and exit
                //BAD_REQUEST (400) being the HTTP response code
                $this->response("Wrong PIN.", 400);
            }
        } else {
            // Set the response and exit
            $this->response("Provide PIN", 400);
        }
    }
}