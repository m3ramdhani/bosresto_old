<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Load the Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Store extends REST_Controller {
    private $_date;

    public function __construct() {
        parent::__construct();
        
        $this->load->model('table_model');
        $this->load->model('categories_model');
        $this->load->model('store_model');
        $this->load->model('order_model');
        $this->load->model('tax_model');
        $this->load->model('cashier_model');
        $this->load->model('options_model');
        $this->load->model('order_menu_inventory_cogs_model');
        $this->load->helper('order');

        $general_setting = $this->store_model->get_general_setting();
        
        foreach ($general_setting as $key => $row) {
           $this->data['setting'][$row->name] = $row->value;
        }

        $this->_date = date("Y-m-d H:i:s");
    }

    public function table_get() {
        $table = $this->table_model->get_data_all_table();
         
        if($table)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Get data table successful.',
                'data' => $table
            ], 200);
        }
 
        else
        {
            $this->response("Get data failed", 400);
        }
    }

    public function categories_get(){
        $categories = $this->categories_model->get_all_categories();
         
        if($categories)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Get data category successful.',
                'data' => $categories
            ], 200);
        }
 
        else
        {
            $this->response("Get data failed", 400);
        }
    }

    public function menus_get($category_id = ''){
        if ($category_id != '') {
            $menus = $this->categories_model->get_menu_by_category($category_id, 'all');
        } else {
            $menus = $this->categories_model->get_all_menus();
        }
         
        if($menus)
        {
            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $menus
            ], 200);
        }
 
        else
        {
            $this->response("Get data failed", 400);
        }
    }

    public function order_by_table_get($table_id = ''){
        $order = $this->store_model->get_order_by_table($table_id);

        if ($order) {
            if (!empty($order)) {
                $order_id = $order->order_id;
            }
    
            $return_data['data_order'] = $this->order_model->get_detail_order($order_id);
    
            if (empty($return_data['data_order'])) {
                redirect(base_url('table'));
            }
    
            $return_data['order_id'] = $order_id;
            $return_data['table_data'] = $this->order_model->get_one('table', $return_data['data_order']->table_id);
            $table_merge = $this->table_model->get_table_merge($return_data['table_data']->id);
            $merge_status = count($table_merge) > 0 ? true : false;
    
            $tax_method = $this->data['setting']['tax_service_method'];
            $get_order_taxes = $this->tax_model->get_taxes(1, $tax_method, 1);
            $order_payment = $this->order_model->calculate_total_order_bill($get_order_taxes, $return_data['order_id']);
            $return_data['order_list'] = $order_payment;
            $return_data['merge_status'] = $merge_status;
    
            $already_process = $this->order_model->paged('order_menu', 1, 0, array(
                'order_id' => $order_id,
                'process_status' => '1'
            ));
    
            $return_data['already_process'] = $already_process->num_rows();
    
            //load content
            $where = array(
                'table_status !=' => '1',
                'table.id !=' => $return_data['data_order']->table_id
            );
            $return_data['list_transfer_table'] = $this->table_model->get_data_all_table($where);
            $return_data['use_role_checker'] = $this->table_model->get_by("master_general_setting", "use_role_checker", "name");

            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $return_data
            ], 200);
        } else {
            $this->response("Get data failed", 400);
        }
    }

    public function side_dish_by_menu_get($menu_id, $is_promo){
        $sidedish = $this->order_model->get_side_dish_by_menu($menu_id, $is_promo);

        if ($sidedish) {
            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $sidedish
            ], 200);
        } else {
            $this->response("Get data failed", 400);
        }
    }

    public function side_dish_by_order_menu_get($order_menu_id, $is_promo){
        $sidedish = $this->order_model->get_side_dish_by_order_menu($order_menu_id, $is_promo);

        if ($sidedish) {
            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $sidedish
            ], 200);
        } else {
            $this->response("Get data failed", 400);
        }
    }

    public function save_order_menu_post()
    {
        $fast_dinein = $this->input->post('fast_dinein');
        $menu_id = $this->input->post('menu_id');
        $order_id = $this->input->post('order_id');
        $count = $this->input->post('count');
        $option = $this->input->post('option');
        $side_dish = $this->input->post('side_dish');
        $notes = $this->input->post('notes');
        $is_edit = $this->input->post('is_edit');
        $order_menu_id = $this->input->post('order_menu_id');
        $outlet_id = $this->input->post('outlet_id');
        $is_take_away = $this->input->post('is_take_away');
        $is_delivery = $this->input->post('is_delivery');
        $dinein_takeaway = $this->input->post('dinein_takeaway');
        $customer_name = $this->input->post('customer_name');
        $order_type = 1;
        if ($fast_dinein == 1) {
            $order_type = 1;
        } elseif ($is_take_away == 1) {
            $order_type = 2;
        } elseif ($is_delivery == 1) {
            $order_type = 3;
        }
        $cooking_status = 0;
        $process_status = 0;
        if ($fast_dinein == 1) {
            $order_id = $this->session->userdata('order_id_dine_in');
            $cooking_status = 0;
            $process_status = 0;
            if ((int) $order_id == 0) {
                $data_fast_order = array(
                    'table_id' => 0,
                    'is_take_away' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                    'created_by' => 0,
                    'has_synchronized' => 0,
                    'reservation_id' => 0,
                    'customer_name' => $customer_name,
                    'start_order' => date("Y-m-d H:i:s")
                );
                $order_id = $this->order_model->save_order($data_fast_order);
            }
        }
        $return_data['status'] = FALSE;
        $return_data['msg'] = "";

        $ingredient_menu_id = $menu_id;
        if ($is_edit == 'true') {
            $old_data_menu = $this->order_model->get_one('order_menu', $order_menu_id);
            $ingredient_menu_id = @$old_data_menu->menu_id;
        }
        $check = true;
        if ($this->data['setting']['zero_stock_order'] == 0) {
            if ($this->data['setting']['stock_menu_by_inventory'] == 1) {
                $menu_ingredient = $this->order_model->one_menu_ingredient_with_stock($ingredient_menu_id);
                $total_available = $menu_ingredient->total_available;
            } else {
                $menu_ingredient = $this->order_model->get_one('menu', $menu_id);
                $total_available = $menu_ingredient->menu_quantity;
                $menu_ingredient->ingredient = $this->order_model->get_menu_ingredient($menu_id);
            }

            if ($count > $total_available) {
                $result = FALSE;
                $return_data['status'] = FALSE;
                $return_data['msg'] = "Gagal menambah pesanan, stok tidak mencukupi. Lakukan refresh halaman.";
                $check = false;
            }
        }
        if ($check == true) {
            if ($is_edit == 'true') {
                $data = array(
                    'quantity' => (@$old_data_menu->quantity + $count),
                    'note' => $notes,
                    'cooking_status' => $cooking_status,
                    'dinein_takeaway' => $dinein_takeaway
                );
                $result = $this->order_model->update_order_menu($order_menu_id, $data);

                $menu_packages = $this->order_model->get_all_where("menu_promo", array("parent_menu_id" => $old_data_menu->menu_id));
                foreach ($menu_packages as $m) {
                    $this->order_model->update_where("order_package_menu", array(
                        "quantity" => $m->quantity * (@$old_data_menu->quantity + $count),
                        "cooking_status" => $cooking_status,
                    ), array("order_menu_id" => $menu_id, "menu_id" => $m->package_menu_id));
                }
            } else {
                $data = array(
                    'menu_id' => $menu_id,
                    'order_id' => $order_id,
                    'quantity' => $count,
                    'note' => $notes,
                    'created_at' => $this->_date,
                    'created_by' => 0,
                    'cooking_status' => $cooking_status,
                    'process_status' => $process_status,
                    'dinein_takeaway' => $dinein_takeaway
                );
                $result = $this->order_model->save_order_menu($data);
                $menu_packages = $this->order_model->get_all_where("menu_promo", array("parent_menu_id" => $menu_id));
                foreach ($menu_packages as $m) {
                    $this->order_model->save("order_package_menu", array(
                        "order_menu_id" => $result,
                        "menu_id" => $m->package_menu_id,
                        "quantity" => $m->quantity * $count,
                        "cooking_status" => $cooking_status,
                        "process_status" => $process_status,
                        "is_check" => 0
                    ));
                }
            }

            if (!empty($side_dish)) $this->order_model->save_side_dish_order_menu($side_dish, $result);
            $this->order_model->save_option_order_menu($option, $result);

        }

        if ($result) {
            $menu_outlet = $this->order_model->get_menu_outlet($outlet_id);
            if ($this->data['setting']['zero_stock_order'] == 0) {
                if ($this->data['setting']['stock_menu_by_inventory'] == 1) {
                    $this->order_model->all_menu_ingredient_with_stock($menu_outlet);
                } else {
                    foreach ($menu_outlet as $m) {
                        $m->total_available = $m->menu_quantity;
                    }
                }
            } else {
                foreach ($menu_outlet as $m) {
                    $m->total_available = 0;
                }
            }

            $tax_method = $this->data['setting']['tax_service_method'];
            $get_order_taxes = $this->tax_model->get_taxes($order_type, $tax_method, 1);
            $order_payment = $this->order_model->calculate_total_order_bill($get_order_taxes, $order_id);

            if (!($is_take_away) && !($is_delivery)) {
                $table = $this->order_model->get_table_by_order_id($order_id);
                $status_name = $this->order_model->get_one('enum_table_status', 4)->status_name;
                if (sizeof($table) > 0) {
                    $data_update = array('table_status' => 4);
                    $this->store_model->update_status_table($table->id, $data_update);


                    $arr_merge = $this->order_model->get_merge_table_byparent($table->id);
                    $this->update_table_merge($arr_merge, $data_update);
                    foreach ($arr_merge as $key => $row) {
                        $row->status_class = create_shape_table($row->table_shape, $status_name);
                    }
                    $return_data['arr_merge_table'] = $arr_merge;
                    $return_data['table_status'] = 4;
                    $return_data['status_name'] = $status_name;
                    $return_data['table_id'] = $table->id;
                    $return_data['table_name'] = $table->table_name;
                    $shape = create_shape_table($table->table_shape, $return_data['status_name']);

                    $return_data['status_class'] = $shape;
                } else {
                    $return_data['arr_merge_table'] = array();
                    $return_data['table_status'] = 4;
                    $return_data['status_name'] = $status_name;
                    $return_data['table_id'] = 0;
                    $return_data['table_name'] = "";
                }
            }


            $return_data['order_id'] = $order_id;
            $return_data['arr_menu_outlet'] = $menu_outlet;
            $return_data['order_list'] = $order_payment;
            $return_data['status'] = TRUE;
            $return_data['count'] = ($this->data['setting']['zero_stock_order'] == 0 ? $menu_ingredient->ingredient : 0);
        }

        if ($return_data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $return_data
            ], 200);
        } else {
            $this->response("Get data failed", 400);
        }
    }

    public function process_order_post()
    {
        $order_id = $this->input->post('order_id');

        $order_proc = $this->order_model->kitchen_prn($order_id);

        $data_order = $this->order_model->get_one('order', $order_id);

        if (!empty($data_order)) {
            $customer_name = $this->input->post('customer_name');
            $customer_phone = $this->input->post('customer_phone');
            $data_customers = array('customer_name' => $customer_name, 'customer_phone' => $customer_phone);

            $data_save = array('table_status' => 3);
            if ($data_order->table_id != 0) {
                $result = $this->order_model->save('table', $data_save, $data_order->table_id);
            } else {
                $result = true;
            }
            if ($this->data['setting']['dining_type'] == 2) {
                $data_update = array(
                    'process_status' => 1,
                    'cooking_status' => 3
                );
            } else {
                if ($this->data['setting']["use_kitchen"] == 1) {
                    $data_update = array(
                        'process_status' => 1,
                        'cooking_status' => 1
                    );
                } else {
                    $data_update = array(
                        'process_status' => 1,
                        'cooking_status' => ($this->data['setting']['use_role_checker'] == 1 ? 7 : 3)
                    );
                }
            }

            $data_update['created_at'] = date("Y-m-d H:i:s");
            $order_menu_new = $this->order_model->get_order_menu(array('om.order_id' => $order_id, 'om.cooking_status' => 0), ($this->data['setting']['dining_type'] == 3) ? TRUE : FALSE);
            $order_package_menus = $this->order_model->get_order_package_menu(array('om.order_id' => $order_id, 'opm.cooking_status' => 0), ($this->data['setting']['dining_type'] == 3) ? TRUE : FALSE);
            $return_data['status_menu'] = [];

            foreach ($order_menu_new as $o) {

                if ($this->data['setting']['stock_menu_by_inventory'] != 1) {
                    $data_menu = $this->order_model->get_one('menu', $o->menu_id);
                    if (!empty($data_menu)) {
                        $this->order_model->save('menu', array('menu_quantity' => $data_menu->menu_quantity - $o->quantity), $o->menu_id);
                    }
                }

                if ($o->is_instant == 1) {
                    $cooking_status = $this->order_model->get_one("enum_cooking_status", 3);
                    $return_data['status_menu'][] = array("id" => $o->id, "cooking_status_name" => $cooking_status->status_name, "cooking_status" => 3);
                    $this->order_model->update_where('order_menu', array(
                        'process_status' => 1,
                        'cooking_status' => 3
                    ), array('id' => $o->id));
                    $this->order_model->update_where("order_package_menu", array(
                        'process_status' => 1,
                        'cooking_status' => 3
                    ), array("order_menu_id" => $o->id));
                } else {
                    if ($data_update['cooking_status'] == 7 && $o->process_checker == 0) {
                        $cooking_status = $this->order_model->get_one("enum_cooking_status", 3);
                        $return_data['status_menu'][] = array("id" => $o->id, "cooking_status_name" => $cooking_status->status_name, "cooking_status" => 3);
                        $this->order_model->update_where('order_menu', array(
                            'process_status' => 1,
                            'cooking_status' => 3
                        ), array('id' => $o->id));
                        $this->order_model->update_where("order_package_menu", array(
                            'process_status' => 1,
                            'cooking_status' => 3
                        ), array("order_menu_id" => $o->id));
                    } else {
                        $cooking_status = $this->order_model->get_one("enum_cooking_status", $data_update['cooking_status']);
                        $return_data['status_menu'][] = array("id" => $o->id, "cooking_status_name" => $cooking_status->status_name, "cooking_status" => $data_update['cooking_status']);
                    }
                }

                if ($this->data['setting']['dining_type'] == 3) {
                    $this->order_model->update_where('order_menu', $data_update, array('order_id' => $order_id, 'cooking_status' => 0, 'id' => $o->id));
                }
            }

            $new_orders = array();
            if ($this->data['setting']["use_kitchen"] == 1) {
                $new_orders = $this->order_model->get_new_order_for_notification(array('order_id' => $order_id, 'cooking_status' => 0, 'process_status' => 0, 'is_instant' => 0));
            }
            $outlets = array();
            $order_menu_remain = $this->order_model->get_all_where('order_menu', array('order_id' => $order_id, 'cooking_status' => 0));
            if ($this->data['setting']['dining_type'] != 3) {
                $this->order_model->update_where('order_menu', $data_update, array('order_id' => $order_id, 'cooking_status' => 0));
            }
            foreach ($order_menu_remain as $remain) {
                $data_update_package_menu = $data_update;
                unset($data_update_package_menu['created_at']);
                $this->order_model->update_where('order_package_menu', $data_update_package_menu, array('order_menu_id' => $remain->id, 'cooking_status' => 0));
            }

            $data_order = $this->order_model->get_data_table($order_id);
            $return_data['notification'] = array();
            foreach ($new_orders as $o) {
                if (!in_array($o->outlet_id, $outlets)) array_push($outlets, (int) $o->outlet_id);
                $order_package_menus = $this->order_model->get_all_where("order_package_menu", array("order_menu_id" => $o->id));
                foreach ($order_package_menus as $p) {
                    $package_menu = $this->order_model->get_one("menu", $p->menu_id);
                    $order_package_menu_category = $this->order_model->get_one("category", $package_menu->category_id);
                    if (!in_array($order_package_menu_category->outlet_id, $outlets)) array_push($outlets, (int) $order_package_menu_category->outlet_id);
                }
                if ($this->data['setting']["notification"] == 1) {
                    $msg = "Pesanan baru menu " . $o->menu_name . " untuk meja " . $data_order->table_name;
                    $all_user = $this->user_model->get_online_all_kitchen(array("outlet_id" => $o->outlet_id));
                    foreach ($all_user as $key => $row) {
                        $data = array(
                            'from_user' => $this->data['user_profile_data']->id,
                            'to_user' => $row->id,
                            'message' => $msg,
                            'seen' => 0,
                            'date' => date("Y-m-d H:i:s")
                        );
                        $notif_id = $this->order_model->save('notification', $data);
                        array_push($return_data['notification'], array(
                            'to_user' => $row->id,
                            'notif_id' => $notif_id,
                            'msg' => $msg
                        ));
                    }
                }
            }
            $return_data['outlets'] = $outlets;

            if ($result) {
                $arr_merge = $this->order_model->get_merge_table_byparent($data_order->table_id);
                $this->update_table_merge($arr_merge, $data_save);
                foreach ($arr_merge as $key => $row) {
                    $row->status_class = create_shape_table($row->table_shape, $data_order->status_name);
                }
                $return_data['arr_merge_table'] = $arr_merge;
                $return_data['number_guest'] = $data_order->customer_count;
                $return_data['table_status'] = ($this->data['setting']["use_kitchen"] == 1 ? 3 : 2);
                $return_data['floor_id'] = $data_order->floor_id;
                $return_data['table_id'] = $data_order->table_id;
                $return_data['table_name'] = $data_order->table_name;
                $return_data['order_id'] = $order_id;
                $return_data['status_name'] = $data_order->status_name;

                $return_data['status_class'] = create_shape_table($data_order->table_shape, $data_order->status_name);
                
                // get bill
                $order_payment = array();
                if (!empty($customer_name)) {
                    $order_payment['customer_data'] = $customer_name;
                } else {
                    $order_payment['table_data'] = $data_order;
                }
                $order_payment['setting'] = $this->data['setting'];

                $this->load->helper(array('printer'));
                $is_print = false;
                if ($this->data['setting']['auto_print'] == 1) {
                    $is_print = true;
                }
                $outlet_id_data = $this->order_model->get_outlet_id_by_order_id($order_id);
                $order_list = $this->order_model->kitchen_order($order_id, TRUE);
                $order_payment['order_list'] = $order_list;
                if (sizeof($order_list) > 0 && $this->data['setting']['auto_checker'] == 1) {
                    $order_payment['DP'] = "DAFTAR PESANAN";

                    if ($order_proc[0]['kitten'] > 0) {
                        $order_payment['DPT'] = "TAMBAHAN";
                    }

                    //get printer checker/service
                    $this->load->model("setting_printer_model");
                    $printer_arr_obj = $this->setting_printer_model->get_printer_by_enum_printer_type(array("name_type" => "printer_checker_service"));
                    foreach ($printer_arr_obj as $value) {
                        //get all tables in setting printer
                        $printer_tables = $this->order_model->get_all_where("setting_printer_table", array('printer_id' => $value->id));
                        foreach ($printer_tables as $key) {
                            //check table in list printer table
                            if ($data_order->table_id == $key->table_id) {
                                //build object printer setting for printer helper
                                $printer_setting = new stdClass();
                                $printer_setting->id = $value->id;
                                $printer_setting->name = $value->alias_name;
                                $printer_setting->value = $value->name_printer;
                                $printer_setting->default = $value->logo;
                                $printer_setting->description = $value->printer_width;
                                
                                //get name alias printer
                                $order_payment['outlet_data'] = $value->alias_name;

                                if ($value->printer_width == 'generic') {
                                    @print_order_kitchen_generic($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                } else {
                                    if ($value->format_order == 1) {
                                        @print_order_kitchen_helper2($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                    } else {
                                        @print_order_kitchen_helper($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                    }
                                }
                            }
                        }
                    }
                }

                $order_menu_ids = array();
                if (sizeof($order_list) > 0) {
                    foreach ($outlet_id_data as $outlet_id) {
                        $order_payment['order_list'] = $this->order_model->kitchen_order($order_id, TRUE, $outlet_id->outlet_id);
                        if (count($order_payment['order_list']) > 0) {
                            foreach ($order_payment['order_list'] as $a) {
                                array_push($order_menu_ids, $a->id);
                            }

                            if ($this->data['setting']['auto_print'] == 1) {
                                $order_payment['DP'] = "DAFTAR PESANAN";

                                if ($order_proc[0]['kitten'] == 0) {
                                    $order_payment['DPT'] = " ";
                                } else {
                                    $order_payment['DPT'] = "TAMBAHAN";
                                }

                                //get printer kitchen
                                $this->load->model("setting_printer_model");
                                $printer_arr_obj = $this->setting_printer_model->get_printer_by_enum_printer_type(array("name_type" => "printer_kitchen"));
                                foreach ($printer_arr_obj as $value) {
                                    //check outlet same with outlet id printer
                                    if ($outlet_id->outlet_id == $value->outlet_id) {
                                        //build object printer setting for printer helper
                                        $printer_setting = new stdClass();
                                        $printer_setting->id = $value->id;
                                        $printer_setting->name = $value->alias_name;
                                        $printer_setting->value = $value->name_printer;
                                        $printer_setting->default = $value->logo;
                                        $printer_setting->description = $value->printer_width;

                                        //get name alias printer
                                        $order_payment['outlet_data'] = $value->alias_name;

                                        if ($value->printer_width == 'generic') {
                                            @print_order_kitchen_generic($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                        } else {
                                            if ($value->format_order == 1) {
                                                @print_order_kitchen_helper2($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                            } else {
                                                @print_order_kitchen_helper($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                            }
                                        }
                                    }
                                }
                            }

                            if ($this->data['setting']['auto_checker_kitchen'] == 1) {
                                //get printer checker kitchen
                                $this->load->model("setting_printer_model");
                                $printer_arr_obj = $this->setting_printer_model->get_printer_by_enum_printer_type(array("name_type" => "printer_checker_kitchen"));
                                foreach ($printer_arr_obj as $value) {
                                    //check outlet same with outlet id printer
                                    if ($outlet_id->outlet_id == $value->outlet_id) {
                                        //build object printer setting for printer helper
                                        $printer_setting = new stdClass();
                                        $printer_setting->id = $value->id;
                                        $printer_setting->name = $value->alias_name;
                                        $printer_setting->value = $value->name_printer;
                                        $printer_setting->default = $value->logo;
                                        $printer_setting->description = $value->printer_width;

                                        //get name alias printer
                                        $order_payment['outlet_data'] = $value->alias_name;

                                        if ($value->printer_width == 'generic') {
                                            @print_order_kitchen_generic($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                        } else {
                                            if ($value->format_order == 1) {
                                                @print_order_kitchen_helper2($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                            } else {
                                                @print_order_kitchen_helper($value->name_printer, $order_payment, $this->data['user_profile_data'], false, $printer_setting);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                foreach ($order_menu_ids as $order_menu_id) {
                    $update_array = array('kitchen_status' => '1');
                    $this->order_model->save('order_menu', $update_array, $order_menu_id);
                }

                $this->response([
                    'status' => TRUE,
                    'message' => 'Get data menu successful.',
                    'data' => $return_data
                ], 200);
                
            } else {
                $this->response("Get data failed", 400);
            }
        } else {
            $this->response("Get data failed", 400);
        }
    }

    public function delete_order_menu_post()
    {
        $menu_id = $this->input->post('order_menu_id');
        $order_id = $this->input->post('order_id');
        $count = $this->input->post('count');

        $order_data = $this->order_model->get_one('order_menu', $menu_id);
        $outlet_id = "";
        if (!empty($order_data)) {
            $outlet_id = $this->order_model->get_outlet_by_menu_id($order_data->menu_id);
        }

        $result = $this->order_model->delete_order_menu($menu_id);
        $this->order_model->delete_by_limit("order_package_menu", array("order_menu_id" => $menu_id), 0);
        $return_data['status'] = FALSE;

        if (!empty($outlet_id)) {
            $menu_outlet = array();
            if ($order_data->cooking_status != 4) {
                $outlet_id = $outlet_id->outlet_id;
                $menu_ingredient = new stdclass();
                $menu_ingredient = $this->order_model->one_menu_ingredient_with_stock($order_data->menu_id);

                $first = array_pop($menu_ingredient->ingredient);
                $ingredients = $this->order_menu_inventory_cogs_model->get_all(array('order_menu_id' => $menu_id));

                if (null != $first->id) {
                    foreach ($ingredients as $ingredient) {
                        $this->order_menu_inventory_cogs_model->delete(array('id' => $ingredient->id));
                    }
                }

                $menu_outlet = $this->order_model->get_menu_outlet($outlet_id);
                if ($this->data['setting']['zero_stock_order'] == 0) {
                    $this->order_model->all_menu_ingredient_with_stock($menu_outlet);
                } else {
                    foreach ($menu_outlet as $m) {
                        $m->total_available = 0;
                    }
                }
            }

            $tax_method = $this->data['setting']['tax_service_method'];
            $get_order_taxes = $this->tax_model->get_taxes(1, $tax_method, 1);
            $order_payment = $this->order_model->calculate_total_order_bill($get_order_taxes, $order_id);
            $return_data['order_list'] = $order_payment;
            $return_data['arr_menu_outlet'] = $menu_outlet;
            $return_data['status'] = TRUE;

            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $return_data
            ], 200);
            
        } else {
            $this->response("Get data failed", 400);
        }
    }

    public function new_order_post()
    {
        $table_id = $this->input->post('table_id');
        $number_guest = $this->input->post('number_guest');
        $reservation_id = $this->input->post('reservation_id');
        $reservation_status = $this->input->post('reservation_status');

        $data_table = $this->order_model->get_one('table', $table_id);

        if (!empty($data_table)) {
            $data_update = array('table_status' => 4, 'customer_count' => $number_guest);
            $this->store_model->update_status_table($table_id, $data_update);
            $check_reservation_menu = $this->order_model->get_all_where("order", array("reservation_id" => $reservation_id, "reservation_id !=" => 0));
            $reservation = $this->order_model->get_one("reservation", $reservation_id);
            if (sizeof($check_reservation_menu) > 0) {
                $reservation = $check_reservation_menu[0];
                $order_id = $reservation->id;
            } else {
                $data_order_where = $this->order_model->get_all_where("order", array("table_id" => $table_id, "order_status" => 0, "start_order <=" => date("Y-m-d H:i:s")));
                if (empty($data_order_where)) {
                    $data = array(
                        'table_id' => $table_id,
                        'is_take_away' => 0,
                        'created_at' => $this->_date,
                        'created_by' => 0,
                        'has_synchronized' => 0,
                        'reservation_id' => $reservation_id,
                        'customer_name' => (sizeof($reservation) > 0 ? $reservation->customer_name : ""),
                        'start_order' => $this->_date
                    );
                    $order_id = $this->order_model->save_order($data);
                } else {
                    $order_id = $data_order_where[ (sizeof($data_order_where) - 1)]->id;
                }

            }

            $status_name = $this->order_model->get_one('enum_table_status', 4)->status_name;
            $arr_merge = $this->order_model->get_merge_table_byparent($table_id);
            $this->update_table_merge($arr_merge, $data_update);
            foreach ($arr_merge as $key => $row) {
                $row->status_class = create_shape_table($row->table_shape, $status_name);
            }

            if (isset($reservation_id) && $reservation_id != 0) {
                $data_reservation = array('status' => $reservation_status, "status_posting" => 1);
                $save = $this->order_model->save('reservation', $data_reservation, $reservation_id);

            }

            $return_data['number_guest'] = $number_guest;
            $return_data['table_status'] = 4;
            $return_data['table_name'] = $data_table->table_name;
            $return_data['table_id'] = $table_id;
            $return_data['order_id'] = $order_id;
            $return_data['status_name'] = $status_name;
            $return_data['arr_merge_table'] = $arr_merge;
            $return_data['arr_menu_outlet'] = FALSE;
            $return_data['status_class'] = create_shape_table($data_table->table_shape, $status_name);

            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $return_data
            ], 200);
        } else {
            $this->response("Get data failed", 400);
        }
    }

    function update_table_merge($array_table, $data_update)
    {
         if(!empty($array_table))
                {
                    foreach ($array_table as $key => $row) {
                         $this->store_model->update_status_table($row->id, $data_update);

                    }
                }
    }

    public function menu_detail_post(){
        $menu_id = $this->input->post('menu_id');
        $menu_order_id = $this->input->post('menu_order_id');
        $menu_data = $this->categories_model->get_one_menu($menu_id);
        $order_menu_data = $this->order_model->get_detail_order_menu($menu_order_id);
        $return_data['order_menu_data'] = $order_menu_data;
        if (!empty($menu_id)) {
            $side_dish = $this->order_model->get_side_dish_by_menu($menu_id, $menu_data->is_promo);
            $sidedishes = array();
            if ($side_dish) {
                foreach ($side_dish as $side) {
                    $sidedish = new stdClass();
                    $sidedish->checked = false;
                    if ($menu_order_id != '0') {
                        $sideOpt = $this->order_model->get_side_dish_by_order_menu($menu_order_id, $menu_data->is_promo);
                        foreach ($sideOpt as $opt) {
                            if ($opt->side_dish_id == $side->id) {
                                $sidedish->checked = true;
                                break;
                            }
                        }
                    }
                    
                    $sidedish->id = $side->id;
                    $sidedish->name = $side->name;
                    $sidedish->price = $side->price;

                    array_push($sidedishes, $sidedish);
                }
                $return_data['side_dish'] = $sidedishes;
            } else {
                $return_data['side_dish'] = $sidedishes;
            }
            
            $this->response([
                'status' => TRUE,
                'message' => 'Get data menu successful.',
                'data' => $return_data
            ], 200);
        } else {
            $this->response("Get data failed", 400);
        }
    }

}