<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_column_account_data_token extends CI_Migration{
public function __construct() {
    $this->load->dbforge();
  }

  public function up() {
    $fields = array(
      'token' => array(
        'type' => 'VARCHAR',
        'constraint' => 50
      )
    );
    $this->dbforge->add_column('account_data', $fields);
  }

  public function down() {
    $this->dbforge->drop_column('account_data', 'token');
  }
  
}
