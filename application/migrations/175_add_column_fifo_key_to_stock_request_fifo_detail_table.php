<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_column_fifo_key_to_stock_request_fifo_detail_table extends CI_Migration {

  public function __construct() {
    $this->load->dbforge();
  }

  public function up() {

    $fields = array(
      'fifo_key'  => array(
        'type'        => 'VARCHAR',
        'constraint'  => 25,
        'default'     => NULL,
        'null'        => TRUE
      )
    );
    $this->dbforge->add_column('stock_request_detail', $fields);
    $this->dbforge->add_column('stock_request_fifo_detail', $fields);
  }

  public function down() {
    $this->dbforge->drop_column('stock_request_detail', 'fifo_key');
    $this->dbforge->drop_column('stock_request_fifo_detail', 'fifo_key');
  }

}