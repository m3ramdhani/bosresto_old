<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_data_account_type extends CI_Migration{

	public function __construct(){
		$this->load->dbforge();
	}
    public function up(){
		$this->db->where("id > 9");
		$this->db->delete("account_type"); 
		
		$this->db->query("ALTER TABLE account_type AUTO_INCREMENT = 9;");
		
		
		$this->db->insert("account_type", array(
			"name" => "Intangible Asset",
			"debit_multiplier" => 1,
			"credit_multiplier" => -1,
			"created_at" => date("Y-m-d H:i:s"),
		));
		$this->db->insert("account_type", array(
			"name" => "Akumulasi Penyusutan Harga Tetap",
			"debit_multiplier" => 1,
			"credit_multiplier" => -1,
			"created_at" => date("Y-m-d H:i:s"),
		));
		$this->db->insert("account_type", array(
			"name" => "Other Non Current Asset",
			"debit_multiplier" => 1,
			"credit_multiplier" => -1,
			"created_at" => date("Y-m-d H:i:s"),
		));
	}

	public function down(){
		$this->db->where("id > 9");
		$this->db->delete("account_type"); 
		
		$check = $this->db->query("ALTER TABLE account_type AUTO_INCREMENT = 9;");
	}
}
