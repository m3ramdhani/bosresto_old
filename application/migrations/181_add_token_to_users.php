<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_token_to_users extends CI_Migration{
public function __construct() {
    $this->load->dbforge();
  }

  public function up() {
    $fields = array(
      'api_token' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      )
    );
    $this->dbforge->add_column('users', $fields);
  }

  public function down() {
    $this->dbforge->drop_column('users', 'api_token');
  }
  
}
