<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_column_account_parent_id extends CI_Migration{
public function __construct() {
    $this->load->dbforge();
  }

  public function up() {
    $fields = array(
      'parent_id' => array(
        'type' => 'integer',  
        'default' => 0,
      )
    );
    $this->dbforge->add_column('account', $fields);
  }

  public function down() {
    $this->dbforge->drop_column('account', 'parent_id');
  }
  
}
