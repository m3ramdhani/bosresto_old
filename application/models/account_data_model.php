<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author      Diky Pratansyah <pratansyah@gmail.com>
 * @copyright   2015 Digital Oasis
 * @since       2.0.0
 */

class Account_Data_Model extends MY_Model{

    /**
     * Table's name to be used throughout the model
     * @var string
     */
    private $_table = 'account_data';

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new data
     * @param   mix     $data    Detail transfer request to be added
     * @return  boolean          Status whether success of failed
     */
    public function add($data) {
        if (count($data) == count($data, COUNT_RECURSIVE)){
            $this->db->insert($this->_table, $data);
            $insert_id = $this->db->insert_id();

            return  $insert_id;
        }else{
            return $this->db->insert_batch($this->_table, $data);
        }
    }

    public function get_all($cond)
    {
        $this->db->select('*')
        ->from($this->_table)
        ->where($cond);
        return $this->db->get()->result();
    }

    public function get_all_no_id($cond)
    {
        $this->db->select('store_id, entry_type, foreign_id, account_id, debit, credit, info, status_ar, created_at, created_by, modified_at, modified_by')
        ->from($this->_table)
        ->where($cond);
        return $this->db->get()->result();
    }
    /**
     * Update table
     * @param  mixed  $data Data to be set
     * @param  mixed  $cond Condition of searching
     * @return boolean
     */
    public function update($data=array(), $cond=array())
    {
        return $this->update_where($this->_table, $data, $cond);
    }
	
    public function add_detail($data) {
        if (count($data) == count($data, COUNT_RECURSIVE))
            return $this->db->insert('account_data_detail', $data);
        else
            return $this->db->insert_batch('account_data_detail', $data);
    }

    public function get_all_detail($cond){
        $this->db->select('*')
        ->from('account_data_detail')
        ->where($cond);
        return $this->db->get()->result();        
    }
	
    public function get_account_receivable($cond = array()) {
        $this->db->select(' account_data.store_id, status_ar, entry_type, foreign_id, account_id, 
                    debit, credit, account_data.created_at, 
                    account.code, account.name, payment_option, bill_payment.info as customer_id ')
                ->distinct()
                ->from('account_data')
                ->join('account', 'account_data.account_id = account.id')
                ->join('bill_payment', 'bill_payment.bill_id = foreign_id')
                ->where($cond)
                ->where('payment_option <>', 1);
        return $this->db->get()->result();
    }

    public function get_bank_distinct() {
        $this->db->select('bank_name')
                ->distinct()
                ->from('bank_account');
        return $this->db->get()->result();
    }

    public function get_bank_store_dropdown($id = 0,$store_id = FALSE){
        $this->db->select('*');
        $this->db->from('bank_account');

        $this->db->where('store_id !=', 0);

        if($store_id){
            $this->db->where('store_id', $store_id);
        }
        
        $this->db->order_by('bank_name', 'ASC');
        $query = $this->db->get();
        $data  = $query->result();

        $results                         = array();
        $results['" class="chain_cl_0'] = "- Pilih Bank -";
        foreach ($data as $row) {
            if ($id != 0 && $id == $row->id) {
                $results[$row->id . '" class="select_me chain_cl_' . $row->store_id] = $row->bank_name;
            }
            else {
                $results[$row->id . '" class="chain_cl_' . $row->store_id] = $row->bank_name;
            }
        }
        return $results;
    }    
	
    public function get_one($id) {
        $this->db->select('*')
                ->from($this->_table)
                ->where('id',$id);
        return $this->db->get()->row();
    }
	
    public function get_profit_loss($cond = array()) {
        $this->db->select(' a.store_id,
                            b.name,b.code, 
                            c.name as type_name,c.id as account_type,a.entry_type,a.created_at,
                            IF(c.debit_multiplier=1,sum(ifnull(a.debit,0))-sum(ifnull(a.credit,0)),sum(ifnull(a.credit,0))-sum(ifnull(a.debit,0))) as jumlah
                            ',false)
                ->from('account_data a')
                ->join('account b', 'a.account_id = b.id')
                ->join('account_type c', 'b.account_type_id = c.id')
                ->where($cond)
                ->group_by("a.account_id");
        return $this->db->get()->result();
    }

    public function get_profit_loss_nodebit($cond = array()) {
        $this->db->select(' a.store_id,
                            b.name,b.code, 
                            c.name as type_name,c.id as account_type,a.entry_type,a.created_at,
                            sum(ifnull(a.credit,0))-sum(ifnull(a.debit,0))
                            as jumlah ',false)
                ->from('account_data a')
                ->join('account b', 'a.account_id = b.id')
                ->join('account_type c', 'b.account_type_id = c.id')
                ->where($cond)
                ->group_by("a.account_id")
                ->group_by("a.id");
                // ->having('sum((c.credit_multiplier * a.credit))  > 0');
        ;
        return $this->db->get()->result();
    }

    public function get_profit_loss_nocredit($cond = array()) {
        $this->db->select(' a.store_id,
                            b.name,b.code, 
                            c.name as type_name,c.id as account_type,a.entry_type,a.created_at,
                            sum((c.debit_multiplier * a.debit)) 
                            as jumlah ')
                ->from('account_data a')
                ->join('account b', 'a.account_id = b.id')
                ->join('account_type c', 'b.account_type_id = c.id')
                ->where($cond)
                ->group_by("a.account_id")->group_by("a.id");
        return $this->db->get()->result();
    }
	
    public function get_general_ledger($params=array())
    {
      if($params['month']<10)$params['month']="0".((int)$params['month']);
      $this->db->select("
        ad.created_at,et.`value`,ad.account_id,`at`.debit_multiplier,ad.debit,ad.credit,a.`code`,a.`name`,(
          SELECT IF(`at`.debit_multiplier=1,SUM(debit)-SUM(credit),SUM(credit)-SUM(debit)) as BALANCE
          FROM account_data
          WHERE account_id=ad.account_id and CONCAT(YEAR(created_at),MONTH(created_at))<'".$params['year'].$params['month']."'
          GROUP BY account_id
        ) as LAST_BALANCE
      ")
      ->from("account_data ad")
      ->join("account a","ad.account_id=a.id","inner")
      ->join("account_type `at`","a.account_type_id=at.id","inner")
      ->join("enum_account_data_entry_type et","ad.entry_type=et.id","inner")
      ->where("ad.store_id",$params['store_id'])
      ->where("year(ad.created_at)",$params['year'])
      ->where("month(ad.created_at)",$params['month'])
      ->order_by("a.code,ad.created_at","asc");
      return $this->db->get()->result();
    }
	
    public function get_trial_balance($params=array())
    {
      if($params['month']<10)$params['month']="0".((int)$params['month']);
      $this->db->select("
        a.code,a.name,`at`.debit_multiplier,(
          SELECT IF(`at`.debit_multiplier=1,SUM(debit)-SUM(credit),SUM(credit)-SUM(debit)) as BALANCE
          FROM account_data
          WHERE account_id=a.id and store_id='".$params['store_id']."' and CONCAT(YEAR(created_at),IF(MONTH(created_at)<10,CONCAT('0',MONTH(created_at)),MONTH(created_at)))<'".$params['year'].$params['month']."'
          GROUP BY account_id
        ) as LAST_BALANCE,
        (
          SELECT SUM(debit)
          FROM account_data
          WHERE account_id=a.id and store_id='".$params['store_id']."'and CONCAT(YEAR(created_at),IF(MONTH(created_at)<10,CONCAT('0',MONTH(created_at)),MONTH(created_at)))='".$params['year'].$params['month']."'
          GROUP BY account_id
        ) as DEBIT,
        (
          SELECT SUM(credit)
          FROM account_data
          WHERE account_id=a.id and store_id='".$params['store_id']."'and CONCAT(YEAR(created_at),IF(MONTH(created_at)<10,CONCAT('0',MONTH(created_at)),MONTH(created_at)))='".$params['year'].$params['month']."'
          GROUP BY account_id
        ) as CREDIT
      ")
      ->from("account a")
      ->join("account_type `at`","a.account_type_id=at.id","inner")
      ->order_by("a.code","asc");
      return $this->db->get()->result();
    }
}
