<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="col-lg-7 pull-left" style="padding-left:0px;">
                <form id="formFilter" method="post">
                    <input type="hidden" id="report_type" value="inventory_price_history" name="type"/>
                    <div class="clear-export"></div>
                    <table class="col-lg-12 form-table-cms">
                        <tbody>
                            <tr>
                                <td><label>Inventory</label></td>
                                <td class="col-sm-8">
                                    <?php 
                                    echo form_dropdown('inventory_id', $inventory_lists, '', 
                                        'id="inventory_id" field-name = "Inventory" 
                                        class="form-control select2" autocomplete="on"');
                                        ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Satuan</label></td>
                                <td class="col-sm-8">
                                    <?php 
                                    echo form_dropdown('uom_id', $uoms_lists, '',
                                        'id="uom_id" field-name = "Satuan" 
                                        class="form-control select2" autocomplete="on" required');
                                        ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Dari Tanggal</label></td>
                                <td class="col-sm-8">
                                    <div class='input-group date ' id='start_purchase_date'>
                                        <?php echo form_input(array('name' => 'start_purchase_date',
                                            'id' => 'start_purchase_date',
                                            'type' => 'text',
                                            'class' => 'form-control date',
                                            'onkeydown'=>'return false',
                                            'value'=>date("Y-m-d")." 00:00",
                                            )); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td><label>Sampai Tanggal</label></td>
                                <td class="col-sm-8">
                                    <div class='input-group date ' id='end_purchase_date'>
                                        <?php echo form_input(array('name' => 'end_purchase_date',
                                            'id' => 'end_purchase_date',
                                            'type' => 'text',
                                            'class' => 'form-control date',
                                            'onkeydown'=>'return false',
                                            'value'=>date("Y-m-d")." 23:59",
                                            )); ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">
                                    <button id="filter_submit" class="btn btn-default "  ><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                                    <button id="export_pdf" type="submit" class="btn btn-success"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Export PDF</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="col-lg-5 pull-right">
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="col-lg-12">
                <div id="inventory_price_history_chart"></div>
            </div>
            <div class="clearfix"></div>
            <table class="table table-striped table-bordered table-hover dt-responsive" cellspacing="0" width="100%" id="table-inventory-price-history">
                <thead>
                    <tr>
                        <th>Inventory</th>
                        <th>Harga Beli</th>
                        <th>Satuan</th>
                        <th>Tanggal Beli</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <input type="hidden" id="dataProcessUrl" value="<?php echo base_url(SITE_ADMIN."/reports/get_data_inventory_price_history"); ?>"/>
            <input type="hidden" id="report_type" value="inventory_price_history"/>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->