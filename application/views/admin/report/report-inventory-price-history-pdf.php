<style>
  table th, table td {
    word-wrap: break-word;
    max-width: 50px;
  }
  .table th
  {
    text-align:center;
  }
  table {
    width: 100%;   
  }
  .bold{
    font-weight:bold;
  }
  th {
    height: 50px;
  }
  table {
    border-collapse: collapse;
  }
  .border{
    margin-bottom:15px;
  }
  .border td, .border th{
    border: solid 1px #000;
    padding-left: 5px;
    padding-right: 5px;
  }
  .text-right{
    text-align:right;
  }
  .text-center{
    text-align:center;
  }
  h4,h5{
    margin-top:3px;
    margin-bottom:3px;
  }
  .is_print{
    font-size:11px;
  }
</style>
<div class="panel-body <?php echo ($is_print==true ? "is_print" : ""); ?>">
  <div class="text-center">
    <h1><?php echo $data_store->store_name;?></h1>
    <h2><label>Laporan Riwayat Harga Barang</label></h2>
    <h3>Periode <?php echo date("d/m/Y",strtotime($from))." s.d. ".date("d/m/Y",strtotime($to)) ;?>
    </h3>
  </div>
  <table class="table table-bordered <?php echo ($is_print==true ? "border" : ""); ?>" >
    <thead>
       <tr>
          <th>Inventory</th>
          <th>Harga Beli</th>
          <th>Satuan</th>
          <th>Tanggal Beli</th>
        </tr>
    </thead>
    <tbody>
    <?php
      foreach($results as $d){ ?>
        <tr>
          <td><?php echo $d->name;?></td>
          <td><?php echo convert_rupiah($d->price);?></td>
          <td><?php echo $d->code;?></td>
          <td><?php echo $d->purchase_date; ?></td>
        </tr>
      <?php }
    ?>
    </tbody>
  </table>
</div>