<style>
    table th, table td {
        word-wrap: break-word;
        max-width: 50px;
    }
    
    th{
        border : 1pt solid black;
    }

    td{
        border : 1pt solid black;
    }
    
    table {
        width: 100%;
    }

    th {
        height: 50px;
    }

    tr.border_bottom td {
      border:1pt solid black;
  }
    
    table {
        border-collapse: collapse;
    }
    
    .border td, .border th{
        border: solid 1px #000;
        padding-left: 5px;
        padding-right: 5px;
    }
    .center {
        margin: auto;
        width: 60%;
        padding: 10px;
    }
</style>
<div class="report_header"style="text-align: center">
    <div>
        <h2 class="center"><label>DAILY SALES REPORT</label></h2>
        <h2 class="center"><label>Cabang : <?php echo isset($store->store_name) ? $store->store_name : "-";?></label></h2>
    </div>
</div>

<div class="panel-body">
    <label>Tanggal : <?php echo $input_date; ?></label>

    <div class="clearfix"></div>

    <table class="table table-striped table-bordered table-hover dt-responsive"  cellspacing="0" id="table-daily-sales">
        <thead>
            <tr>
                <th class="text-center" rowspan="2" width="5%">No.</th>
                <th class="text-center" rowspan="2" width="30%">Pendapatan</th>
                <th class="text-center" colspan="2" >Today</th>
                <th class="text-center" colspan="2">Month to Date</th>
            </tr>
            <tr>
                <th class="text-center" width="15%">Total</th>
                <th class="text-center" width="10%">%</th>
                <th class="text-center" width="15%">Total</th>
                <th class="text-center" width="10%">%</th>
            </tr>
        </thead>
       
        <tbody id="table_body">
        <?php echo $table; ?>
        </tbody>
    </table>
    <!-- /.table-responsive -->
</div>
