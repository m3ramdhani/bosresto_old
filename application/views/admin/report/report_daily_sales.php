<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
<form id="formFilter" method="get" target="_blank">
  <input type="hidden" id="report_type" value="daily_report" name="type"/>
    <div class="clear-export"></div>
    <table class="col-lg-8 form-table-cms">
        <tbody>
            <tr>
                <td><label>Resto</label></td>
                <td class="col-sm-8">
                   <?php 
                                echo form_dropdown('store_id', $all_store, $user_store['store_selected'],
                                'id="store_id" field-name = "Resto" 
                                class="form-control" autocomplete="on" '.$user_store['disabled_dropdown']);
                    ?>                       
                </td>                        
            </tr>
            <tr>
                <td><label>Tanggal</label></td>
                <td class="col-sm-8">
                    <div class='input-group date ' id='input_date'>
                         <?php echo form_input(array('name' => 'input_date',
                           'id' => 'input_date',
                           'type' => 'text',
                           'class' => 'form-control date',
                           'onkeydown'=>'return false',
                           'value'=>date("Y-m-d")

                           )); ?>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar">
                            </span>
                        </span>
                    </div> 
                </td>                  
            </tr>
            <tr>
                <td colspan="4" align="right">
                    <!-- <button id="export_xls" class="btn btn-default hide_btn" style="float:right;display: none">Export XLS</button> -->
                    <button id="filter_submit" class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                    <button id="export_pdf" class="btn btn-success hide_btn" style="display: none"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Export PDF</button>
                    
                </td>
            </tr>
        </tbody>
    </table>
</form>

            <div class="clearfix"></div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

            <div class="clearfix"></div>

            <table class="table table-striped table-bordered table-hover dt-responsive"  cellspacing="0" id="table-daily-sales">
                <thead>
                    <tr>
                        <th class="text-center" rowspan="2" width="5%">No.</th>
                        <th class="text-center" rowspan="2" width="30%">Pendapatan</th>
                        <th class="text-center" colspan="2" >Today</th>
                        <th class="text-center" colspan="2">Month to Date</th>
                    </tr>
                    <tr>
                        <th class="text-center" width="15%">Total</th>
                        <th class="text-center" width="10%">%</th>
                        <th class="text-center" width="15%">Total</th>
                        <th class="text-center" width="10%">%</th>
                    </tr>
                </thead>
               
                <tbody id="table_body">
                </tbody>
            </table>

            <input type="hidden" id="dataProcessUrl"
                   value="<?php echo $data_url; ?>"/>

            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>

<!-- /.col-lg-12 -->

<!-- /.panel-hidden -->
<div class="panel-hidden" style="display:none">

    <h1 class="page-header"><?php echo $this->lang->line('sales_report_title'); ?></h1>
    <table class="col-lg-12 form-table-cms" style="width:100%;">
        <tbody>
        <tr>
            <td><label><?php echo $this->lang->line('column_start_date'); ?></label></td>
            <td>:</td>
            <td><label id='start_date_hidden'></label></td>
            <td><label> <?php echo $this->lang->line('column_store'); ?></label></td>
            <td>:</td>
            <td><label id='store_hidden'></label></td>
        </tr>
        <tr>
            <td><label><?php echo $this->lang->line('column_end_date'); ?></label></td>
            <td>:</td>
            <td><label id='end_date_hidden'></label></td>
            <td><label><?php echo $this->lang->line('column_outlet'); ?></label></td>
            <td>:</td>
            <td><label id='outlet_hidden'></label></td>
        </tr>
        <tr>
            <td><label><?php echo $this->lang->line('column_date_month'); ?></label></td>
            <td>:</td>
            <td><label id='month_hidden'></label></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label><?php echo $this->lang->line('column_date_year'); ?></label></td>
            <td>:</td>
            <td><label id='year_hidden'></label></td>
            <td></td>
            <td></td>
        </tr>

         <tr>
            <td><label>payment</label></td>
            <td>:</td>
            <td><label id='payment_hidden'></label></td>
            <td></td>
            <td></td>
        </tr>

        </tbody>
    </table>

    <table id="table-sales-hidden" class="sales-table" style="border-collapse: collapse;width:100%;">
        <thead class="custom-hidden-table">
        </thead>
    </table>

    <!-- /.table-responsive -->
</div>
<!-- /.panel-hidden -->