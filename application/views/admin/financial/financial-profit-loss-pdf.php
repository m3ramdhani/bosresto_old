	<style>
		table th, table td {
			word-wrap: break-word;
			max-width: 50px;
		}
		
	/*	th {
			background-color: #3CB371;
			color: #fff;
		}*/
		
		table {
			width: 100%;
           
		}

		th {
			height: 50px;
		}
		
		table {
			border-collapse: collapse;
		}
		
		.border td, .border th{
			border: solid 1px #000;
			padding-left: 5px;
			padding-right: 5px;
		}
        .table-data{
            margin-left:24px;width:100%;
        }
        .laba-hightlight{
            color: red;
        }
        td .codename{
            width:90%;
        }
        td .money{
            float:left;
        }
        .parent-table{
            border-bottom:1px solid black;
        }
	</style>
 

    <div class="panel-body">
        <table class="table" style="">
            <tbody>
                
                <tr>
                    <td align="center" colspan="2">
                   
                    <h5><?php echo @$data_store[0]->store_name;?></h5>
                    <h4><label>Laporan Laba Rugi</label></h4>
                    <h5>Per <?php echo $month;?> <?php echo $year;?></h5>
                    </td>
                </tr>
                
                <tr>
                 <td align="left"> </td>
                 <td align="right"><h4>Saldo</h4></td>
                </tr>

                
            </tbody>
        </table>
    
        <table class="table parent-table" id="pendapatan_usaha" >
            <?php $total_pendapatan_usaha = 0;?>
            <tbody>
                
                <tr>
                    <td  colspan="2">Pendapatan</td>
                </tr>
                <tr>
                    <td  colspan="2"><div style="margin-left:12px;">Pendapatan Usaha</div></td>
                </tr>
                <?php foreach ($get_profit_loss_income as $profit) { ?>
                  <tr>
                      <td class="codename" style="padding-left:24px;"><?php echo $profit->code;?> - <?php echo $profit->name;?></td>
                      <td align="right">
                          <?php 
                              echo convert_rupiah($profit->jumlah);  
                              $total_pendapatan_usaha += $profit->jumlah; 
                          ?> 
                      </td>
                  </tr>
                <?php }  ?>
                <?php /*
                 <tr>
                     <td colspan="2">
                          <table  class="table-data">
                            <tbody>
                                <?php foreach ($get_profit_loss_income as $profit) { ?>
                                    <tr>
                                        <td class="codename"><?php echo $profit->code;?> - <?php echo $profit->name;?></td>
                                        <td>
                                            <?php 
                                                echo convert_rupiah($profit->jumlah);  
                                                $total_pendapatan_usaha += $profit->jumlah; 
                                            ?> 
                                        </td>
                                    </tr>
                                <?php }  ?>
                            </tbody>
                        </table>

                    </td>
                </tr>*/ ?>
                 <tr>
                    <td  ><div style="margin-left:12px;;">Total Pendapatan Usaha</div></td>
                     <td align="right"><?php echo convert_rupiah($total_pendapatan_usaha);?> </td>
                </tr>
                <tr>
                    <td>Total Pendapatan</td>
                    <td align="right">
                    <?php 
                        echo convert_rupiah($total_pendapatan_usaha);
                    ?> 
                    </td>
                </tr>
               
            </tbody>
        </table>

        <table class="table parent-table" id="biaya_atas_pendapatan">
         <?php $total_biaya_usaha = 0;?>
            <tbody>
                
                <tr>
                    <td  colspan="2">Biaya Atas Pendapatan</td>
                </tr>
                <tr>
                    <td  colspan="2"><div style="margin-left:12px;;">Biaya Usaha</div></td>
                </tr>
                  <tr>
                     <td colspan="2">
                          <table  class="table-data">
                            <tbody>
                                <?php foreach ($get_profit_loss_incomecost as $usaha) { ?>
                                    <tr>
                                        <td class="codename"><?php echo $usaha->code;?> - <?php echo $usaha->name;?></td>
                                        <td  ><?php echo convert_rupiah($usaha->jumlah);  $total_biaya_usaha += $usaha->jumlah;  ?> </td>
                                    </tr>
                                <?php }?>
                              
                                  
                            </tbody>
                        </table>

                    </td>
                </tr>
                   
                 <tr>
                    <td  ><div style="margin-left:12px;;">Total Biaya Usaha</div></td>
                     <td align="right"><?php echo convert_rupiah($total_biaya_usaha);?> </td>
                </tr>
                <tr>
                    <td  >Total Biaya Atas Pendapatan</td>
                     <td align="right"><?php echo convert_rupiah($total_biaya_usaha);?> </td>
                </tr>
                <tr class="laba-hightlight">
                    <td  >Laba / Rugi Kotor</td>
                     <td align="right"><?php 
                        $laba_kotor = 0;
                        $laba_kotor = $total_pendapatan_usaha - $total_biaya_usaha;
                        echo convert_rupiah($laba_kotor);
                      ?> 
                     </td>
                </tr>
            </tbody>
        </table>


        <table class="table parent-table"  id="pengeluaran_operational">
            <?php $total_pengeluaran_operational = 0;
                $total_pengeluaran_non_operational = 0;
            ?>
            <tbody>
                 
                <tr>
                    <td  colspan="2">Pengeluaran Operasional</td>
                </tr>
                <tr>
                    <td  colspan="2"><div style="margin-left:12px;;">Biaya Operasional</div></td>
                </tr>
                 <tr>
                    <td colspan="2">
                        <table  class="table-data">
                            <tbody>
                                <?php foreach ($get_profit_loss_operational as $profit) { ?>
                                    <tr>
                                        <td class="codename"><?php echo $profit->code;?> - <?php echo $profit->name;?></td>
                                        <td  >
                                            <?php echo convert_rupiah($profit->jumlah);  
                                            $total_pengeluaran_operational += $profit->jumlah;  ?> 
                                        </td>
                                    </tr>
                                <?php }?>
                              
                                  
                            </tbody>
                        </table>

                    </td>
                </tr>
                 <tr>
                    <td  ><div style="margin-left:12px;;">Total Biaya Operasional</div></td>
                     <td align="right"><?php echo convert_rupiah($total_pengeluaran_operational); ?> </td>
                </tr>

                <tr>
                    <td  colspan="2"><div style="margin-left:12px;;">Biaya Non Operasional</div></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table  class="table-data">
                            <tbody>
                                <?php foreach ($get_profit_loss_non_operational as $profit) { ?>
                                    <tr>
                                        <td class="codename"><?php echo $profit->code;?> - <?php echo $profit->name;?></td>
                                        <td class="money">
                                            <?php 
                                            echo convert_rupiah($profit->jumlah); 
                                            $total_pengeluaran_non_operational += $profit->jumlah; 
                                             ?> 
                                        </td>
                                    </tr>
                                <?php }?>
                              
                                  
                            </tbody>
                        </table>

                    </td>
                </tr>
                 <tr>
                    <td  ><div style="margin-left:12px;;">Total Biaya Non Operasional</div></td>
                     <td align="right"><?php echo convert_rupiah($total_pengeluaran_non_operational); ?> </td>
                </tr>
                <tr>
                    <td  >Total Pengeluaran Operasional</td>
                      <td align="right">
                        <?php  
                            $grand_total_operasional = 0; 
                            $grand_total_operasional = $total_pengeluaran_operational + $total_pengeluaran_non_operational; 
                            echo convert_rupiah($grand_total_operasional);
                        ?> 
                    </td>
                </tr>
                <tr class="laba-hightlight">
                    <td  >Laba / Rugi Operasional</td>
                    <td align="right"><?php 
                        $laba_rugi_operasional = 0;
                        $total_laba_operasional = 0;
                        $laba_rugi_operasional = $total_pengeluaran_operational + $total_pengeluaran_non_operational;
                        $total_laba_operasional = $laba_kotor - $laba_rugi_operasional;
                        echo ($laba_rugi_operasional == 0)?convert_rupiah(0):convert_rupiah($total_laba_operasional);
                    ?> 
                     </td>
                </tr>
            </tbody>
        </table>

        <table class="table parent-table" id="pendapatan_lain">
        <?php $total_income = 0; ?>
            <tbody>
                
                <tr>
                    <td  colspan="2">Pendapatan Lain-lain</td>
                </tr>
                <tr>
                    <td  colspan="2"><div style="margin-left:12px;;">Pendapatan Di luar Usaha</div></td>
                </tr>
                 <tr>
                   <td colspan="2">
                        <table  class="table-data">
                            <tbody>
                                <?php foreach ($get_profit_loss_outcome as $profit) { ?>
                                    <tr>
                                        <td class="codename"><?php echo $profit->code;?> - <?php echo $profit->name;?></td>
                                        <td  >
                                            <?php 
                                                echo convert_rupiah($profit->jumlah);  
                                                $total_income += $profit->jumlah; 
                                            ?> 
                                        </td>
                                    </tr>
                                <?php }?>
                              
                                  
                            </tbody>
                        </table>

                    </td>
                </tr>
                 <tr>
                    <td  ><div style="margin-left:12px;;">Total Pendapatan Di luar Usaha</div></td>
                     <td align="right"><?php echo convert_rupiah($total_income); ?> </td>
                </tr>
                <tr>
                    <td  >Total Pendapatan Lain lain</td>
                     <td align="right"><?php echo convert_rupiah($total_income); ?> </td>
                </tr>
                <tr class="laba-hightlight">
                    <td  >Laba / Rugi Bersih</td>
                    <td align="right"><?php 
                       $laba_bersih = 0;

                       $laba_bersih = $total_laba_operasional + $total_income;
                       echo convert_rupiah($laba_bersih);
                    ?> 
                     </td>
                </tr>
            </tbody>
        </table>
    </div>