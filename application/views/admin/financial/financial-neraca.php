<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
    <form id="formFilter" method="get" target="_blank">
    <input type="hidden" id="financial_type" value="neraca" name="type"/>
    <div class="clear-export"></div>
            <table class="col-lg-8 form-table-cms">
                <tbody>
                    <tr>
                        <td><label>Resto</label></td>
                        <td class="col-sm-8">
                           <?php 
                                echo form_dropdown('store_id', $all_store, $user_store['store_selected'],
                                'id="store_id" field-name = "Resto" 
                                class="form-control" autocomplete="on" '.$user_store['disabled_dropdown']);
                            ?>
                            
                        </td>
                        
                    </tr>
                    <tr>
                        <td><label>Bulan</label></td>
                        <td class="col-sm-8">
                            <div class='input-group date '  >
                             <?php echo form_input(array('name' => 'month_year',
                               'id' => 'month_year',
                               'type' => 'text',
                               'class' => 'form-control date', 
                               'onkeydown'=>'return false',
                               'value'=>date("m-Y")

                               )); ?>
                               <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar">
                                </span>
                            </span>
                        </div> 
                    </td>
                
                </tr>
                
                
               
                <tr>
                    <td colspan="4" align="right">

                        <button id="filter_submit" class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                        <button id="export_pdf" class="btn btn-success hide_btn" style="display: none"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Export PDF</button>
                        
                    </td>
                </tr>
                </tbody>
            </table>
</form>

            <div class="clearfix"></div>
        </div>

<div id="financial_content">
    <style>
        table th, table td {
            word-wrap: break-word;
            max-width: 50px;
        }
        
    /*  th {
            background-color: #3CB371;
            color: #fff;
        }*/
        
        table {
            width: 100%;
           
        }

        th {
            height: 50px;
        }
        
        table {
            border-collapse: collapse;
        }
        
        .border td, .border th{
            border: solid 1px #000;
            padding-left: 5px;
            padding-right: 5px;
        }
        .table-data{
            margin-left:24px;width:100%;
        }
        .laba-hightlight{
            color: red;
        }
        td .codename{
            width:95%;
        }
        td .money{
            float:left;
        }
        .parent-table{
            
            width:80%;
            margin: auto;
        }
        .footer-table{
            border-top:1px solid black !important;
        }
        .text-right{
            text-align: right;
        }
        .right-panel{
           padding-left:15px;
        }
    </style>
 

    <div class="panel-body">
        <table class="table" style="">
            <tbody>
                
                <tr>
                    <td align="center" colspan="2">
                   
                    <h5><?php echo @$data_store[0]->store_name;?></h5>
                    <h4><label>Laporan Keuangan Neraca</label></h4>
                    <h5>Per <?php echo $month;?> <?php echo $year;?></h5>
                    </td>
                </tr>  
            </tbody>
        </table>
    
        <table class="  parent-table" id="pendapatan_usaha" >
            <?php $total_pendapatan_usaha = 0;?>
            <tbody>
                <tr>
                    <td  class="codename"><h4>Aktiva Lancar</h4></td>
                   
                    <td  class="right-panel">
                       <h4> Kewajiban</h4>
                    </td>
                </tr>
                <tr>
                    <td  class="codename"> 
                        <table  >
                            <?php 
                            $total_current_asset = 0;
                            foreach ($get_current_asset as $current) { ?>
                                <tr>
                                    <td style="width:50%;">
                                        <?php echo $current->name;?>
                                    </td>
                                     <td>
                                       Rp.
                                    </td>
                                     <td class="text-right">
                                        <?php echo convert_rupiah_report($current->jumlah);
                                        $total_current_asset += $current->jumlah;
                                        ?>
                                    </td>
                                </tr>
                            <?php }?> 
                        </table>

                    </td>
                    <td align="right"  class="right-panel">
                         <table  >
                            <?php 
                            $total_liability = 0;
                            foreach ($get_liability as $current) { ?>
                                <tr>
                                    <td style="width:50%;">
                                        <?php echo $current->name;?>
                                    </td>
                                     <td>
                                       Rp.
                                    </td>
                                     <td class="text-right">
                                        <?php echo convert_rupiah_report($current->jumlah);
                                        $total_liability += $current->jumlah;
                                        ?>
                                    </td>
                                </tr>
                            <?php }?>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td  class="codename">
                        <table  >
                            <tr>
                                <td  style="width:50%;">
                                  Jumlah Aktiva Lancar
                                </td>
                                  <td  >
                                    Rp
                                </td>
                                 <td class="text-right">
                                      <?php echo convert_rupiah_report($total_current_asset);?>
                                </td>
                            </tr>
                        </table> 
                    </td>
                    <td  class="right-panel">
                        <table  >
                            <tr>
                                <td  style="width:50%;">
                               Jumlah Kewajiban
                                </td>
                                  <td  >
                                    Rp
                                </td>
                                 <td class="text-right">
                                      <?php echo convert_rupiah_report($total_liability);?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td  class="codename"><h4>Aktiva Tetap</h4></td>
                   
                    <td  class="right-panel">
                        <h4>Modal</h4>
                    </td>
                </tr>
                <tr>
                    <td  class="codename"> 
                        <table  >
                            <?php 
                            $total_fixed_asset = 0;
                            foreach ($get_fixed_asset as $current) { ?>
                                <tr>
                                    <td style="width:50%;">
                                        <?php echo $current->name;?>
                                    </td>
                                     <td>
                                       Rp.
                                    </td>
                                     <td class="text-right">
                                        <?php echo convert_rupiah_report($current->jumlah);
                                        $total_fixed_asset += $current->jumlah;
                                        ?>
                                    </td>
                                </tr>
                            <?php }?>
                        </table>
                    </td>
                   
                    <td align="right"  class="right-panel">
                         <table  >
                           <?php 
                           $total_first_modal = 0;
                           foreach ($get_first_modal as $current) { ?>
                                <tr>
                                    <td style="width:50%;">
                                        <?php echo $current->name;?>
                                    </td>
                                     <td>
                                       Rp.
                                    </td>
                                     <td class="text-right">
                                        <?php echo convert_rupiah_report($current->jumlah);
                                        $total_first_modal += $current->jumlah;
                                        ?>
                                    </td>
                                </tr>
                            <?php }?>
                            <tr>
                              <td style="width:50%;">
                                  Laba Berjalan
                              </td>
                               <td>
                                 Rp.
                              </td>
                               <td class="text-right">
                                <?php 
                                  echo convert_rupiah_report($last_profit_loss);
                                  $total_first_modal += $last_profit_loss;
                                ?>
                              </td>
                          </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td  class="codename">
                        <table  >
                            <tr>
                                <td  style="width:50%;">
                                  Jumlah Aktiva Tetap
                                </td>
                                  <td  >
                                    Rp
                                </td>
                                 <td class="text-right">
                                      <?php echo convert_rupiah_report($total_fixed_asset);?>
                                </td>
                            </tr>
                        </table> 
                    </td>
                    <td  class="right-panel">
                        <table  >
                            <tr>
                                <td  style="width:50%;">
                               Jumlah Modal
                                </td>
                                  <td  >
                                    Rp
                                </td>
                                 <td class="text-right">
                                      <?php echo convert_rupiah_report($total_first_modal);?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td  class="codename footer-table" >
                         <table  >
                            <tr>
                                <td  style="width:50%;">
                                   <h4> Total Aktiva </h4>
                                </td>
                                  <td  >
                                    Rp
                                </td>
                                 <td class="text-right">
                                      <?php 
                                      $total_activa = 0;
                                      $total_activa = $total_current_asset + $total_fixed_asset;
                                      echo convert_rupiah_report($total_activa);?>
                                </td>
                            </tr>
                        </table>

                    </td>
                    
                    <td align="right" class="footer-table right-panel">
                     <table  >
                            <tr>
                                <td style="width:50%;" >
                                  <h4>  Total Pasiva</h4>
                                </td>
                                  <td  >
                                    Rp
                                </td>
                                 <td class="text-right">
                                    <?php 
                                    $total_pasiva = 0;
                                    $total_pasiva = $total_liability + $total_first_modal;
                                    echo convert_rupiah_report($total_pasiva);?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
 
    </div>
</div>
       

            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
 
