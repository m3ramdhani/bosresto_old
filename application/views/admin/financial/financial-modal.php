<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
    <form id="formFilter" method="get" target="_blank">
    <input type="hidden" id="financial_type" value="modal" name="type"/>
    <div class="clear-export"></div>
            <table class="col-lg-8 form-table-cms">
                <tbody>
                    <tr>
                        <td><label>Resto</label></td>
                        <td class="col-sm-8">
                           <?php 
                                echo form_dropdown('store_id', $all_store, $user_store['store_selected'],
                                'id="store_id" field-name = "Resto" 
                                class="form-control" autocomplete="on" '.$user_store['disabled_dropdown']);
                ?>
                            
                        </td>
                        
                    </tr>
                    <tr>
                        <td><label>Bulan</label></td>
                        <td class="col-sm-8">
                            <div class='input-group date '  >
                             <?php echo form_input(array('name' => 'month_year',
                               'id' => 'month_year',
                               'type' => 'text',
                               'class' => 'form-control date', 
                               'onkeydown'=>'return false',
                               'value' => date("m-Y")

                               )); ?>
                               <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar">
                                </span>
                            </span>
                        </div> 
                    </td>
                
                </tr>
                
                
               
                <tr>
                    <td colspan="4" align="right">

                        <button id="filter_submit" class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                        <button id="export_pdf" class="btn btn-success hide_btn" style="display: none"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Export PDF</button>
                        
                    </td>
                </tr>
                </tbody>
            </table>
</form>

            <div class="clearfix"></div>
        </div>

<div id="financial_content">
    <style>
        table th, table td {
            word-wrap: break-word;
            max-width: 50px;
        }
        
    /*  th {
            background-color: #3CB371;
            color: #fff;
        }*/
        
        table {
            width: 100%;
           
        }

        th {
            height: 50px;
        }
        
        table {
            border-collapse: collapse;
        }
        
        .border td, .border th{
            border: solid 1px #000;
            padding-left: 5px;
            padding-right: 5px;
        }
        .table-data{
            margin-left:24px;width:100%;
        }
        .laba-hightlight{
            color: red;
        }
        td .codename{
            width:95%;
        }
        td .money{
            float:left;
        }
        .parent-table{
            
            width:60%;
            margin: auto;
        }
        .footer-table{
            border-top:1px solid black !important;
        }
    </style>
 

    <div class="panel-body">
        <table class="table" style="">
            <tbody>
                
                <tr>
                    <td align="center" colspan="2">
                   
                    <h5><?php echo @$data_store[0]->store_name;?></h5>
                    <h4><label>Laporan Laba Berjalan</label></h4>
                    <h5>Per <?php echo $month;?> <?php echo $year;?></h5>
                    </td>
                </tr>  
            </tbody>
        </table>
    
        <table class="table parent-table" id="pendapatan_usaha" >
            <?php $total_pendapatan_usaha = 0;?>
            <tbody>
                <tr>
                    <td  class="codename">Modal Awal </td>
                     <td align="right">Rp</td>
                     <td></td>
                    <td align="right">
                        <?php 
                        $total_modal = $last_profit_loss;
                      
                        foreach ($get_first_modal as $modal) {
                              $total_modal += $modal->jumlah;

                        }
                        echo convert_rupiah_report($total_modal);?>
                    </td>
                </tr>
                <tr>
                    <td   class="codename">Laba Bersih</td>
                     <td align="right">Rp</td>
                    <td align="right">
                   <?php echo convert_rupiah_report($total_laba_bersih);?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td  class="codename" >Prive</td>
                     <td align="right">Rp</td>
                    <td align="right">
                    <?php 
                        $total_prive = 0;
                      
                        foreach ($data_prive as $prive) {
                              $total_prive += $prive->jumlah;

                        }
                        echo convert_rupiah_report($total_prive);?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td  class="codename" >Penambahan Modal</td>
                    <td  align="right">Rp</td>
                    <td></td>
                    <td align="right">
                   <?php 
                   $total_penambahan_modal = $total_laba_bersih - abs($total_prive);
                   echo convert_rupiah_report($total_penambahan_modal);?>
                    </td>
                </tr>
               <tr>
                    <td  class="codename footer-table" >Modal Akhir</td>
                    <td align="right" class="footer-table">Rp</td>
                    <td class="footer-table"></td>
                    <td align="right" class="footer-table">
                   <?php  
                   $total_modal_akhir = $total_modal + $total_penambahan_modal;
                   echo convert_rupiah_report($total_modal_akhir);?>
                    </td>
                </tr>
            </tbody>
        </table>
 
    </div>
</div>
       

            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
 
