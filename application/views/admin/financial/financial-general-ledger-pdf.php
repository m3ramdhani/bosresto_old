<style>
  table th, table td {
    word-wrap: break-word;
    max-width: 50px;
  }
  .table th
  {
    text-align:center;
  }
  table {
    width: 100%;   
  }
  .bold{
    font-weight:bold;
  }
  th {
    height: 50px;
  }
  table {
    border-collapse: collapse;
  }
  .border{
    margin-bottom:15px;
  }
  .border td, .border th{
    border: solid 1px #000;
    padding-left: 5px;
    padding-right: 5px;
  }
  .text-right{
    text-align:right;
  }
  .text-center{
    text-align:center;
  }
  h4,h5{
    margin-top:3px;
    margin-bottom:3px;
  }
  .is_print{
    font-size:11px;
  }
</style>
<div class="panel-body <?php echo ($is_print==true ? "is_print" : ""); ?>">
    <div class="text-center">
      <h5><?php echo @$data_store[0]->store_name;?></h5>
      <h4><label>Laporan Buku Besar</label></h4>
      <h5>Per <?php echo $month;?> <?php echo $year;?></h5>
    </div>
      <?php 
        foreach($coa_ledgers as $c){
      ?>
      <h5 class="bold"><?php echo $c->name." ( ".$c->code." )" ?></h5>
      <table class="table table-bordered <?php echo ($is_print==true ? "border" : ""); ?>" >
      <thead>
        <tr>
          <th>Tanggal</th>
          <th>Entry Type</th>
          <th>Debit</th>
          <th>Credit</th>
          <th>Saldo</th>
        </tr>
      </thead>
      <tbody>
      <?php
          echo '<tr>';
          echo '<td colspan="4">Saldo Awal</td>';
          echo '<td class="text-right">'.convert_rupiah($c->LAST_BALANCE).'</td>';
          echo '</tr>';
          $total_debit=0;
          $total_credit=0;
          $balance=$c->LAST_BALANCE;
          foreach($results[$c->account_id] as $d){
            $total_debit+=$d->debit;
            $total_credit+=$d->credit;
            if($c->debit_multiplier==1){
              $balance+=$d->debit-$d->credit;
            }else{
              $balance+=$d->credit-$d->debit;
            }
            echo '<tr>';
            echo '<td>'.date("d/m/Y H:i:s",strtotime($d->created_at)).'</td>';
            echo '<td>'.$d->value.'</td>';
            echo '<td class="text-right">'.convert_rupiah($d->debit).'</td>';
            echo '<td class="text-right">'.convert_rupiah($d->credit).'</td>';
            echo '<td class="text-right">'.convert_rupiah($balance).'</td>';
            echo '</tr>';
          }
          echo '<tr class="bold">';
          echo '<td colspan="2" class="text-center">Sub Total</td>';
          echo '<td class="text-right">'.convert_rupiah($total_debit).'</td>';
          echo '<td class="text-right">'.convert_rupiah($total_credit).'</td>';
          echo '<td class="text-right">'.convert_rupiah($balance).'</td>';
          echo '</tr>';
          echo '</tbody>';
          echo '</table>';
        }
      ?>
</div>