	<style>
		table th, table td {
			word-wrap: break-word;
			max-width: 50px;
		}
		
	/*	th {
			background-color: #3CB371;
			color: #fff;
		}*/
		
		table {
			width: 100%;
           
		}

		th {
			height: 50px;
		}
		
		table {
			border-collapse: collapse;
		}
		
		.border td, .border th{
			border: solid 1px #000;
			padding-left: 5px;
			padding-right: 5px;
		}
        .table-data{
            margin-left:24px;width:100%;
        }
        .laba-hightlight{
            color: red;
        }
        td .codename{
            width:95%;
        }
        td .money{
            float:left;
        }
        .parent-table{
            
            width:60%;
            margin: auto;
        }
        .footer-table{
            border-top:1px solid black !important;
        }
	</style>
 

    <div class="panel-body">
        <table class="table" style="">
            <tbody>
                
                <tr>
                    <td align="center" colspan="2">
                   
                    <h5><?php echo @$data_store[0]->store_name;?></h5>
                    <h4><label>Laporan Laba Berjalan</label></h4>
                    <h5>Per <?php echo $month;?> <?php echo $year;?></h5>
                    </td>
                </tr>  
            </tbody>
        </table>
    
        <table class="table parent-table" id="pendapatan_usaha" >
            <?php $total_pendapatan_usaha = 0;?>
            <tbody>
                <tr>
                    <td  class="codename">Modal Awal </td>
                     <td align="right">Rp</td>
                     <td></td>
                    <td align="right">
                        <?php 
                        $total_modal = $last_profit_loss;
                      
                        foreach ($get_first_modal as $modal) {
                              $total_modal += $modal->jumlah;

                        }
                        echo convert_rupiah_report($total_modal);?>
                    </td>
                </tr>
                <tr>
                    <td   class="codename">Laba Bersih</td>
                     <td align="right">Rp</td>
                    <td align="right">
                   <?php echo convert_rupiah_report($total_laba_bersih);?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td  class="codename" >Prive</td>
                     <td align="right">Rp</td>
                    <td align="right">
                    <?php 
                        $total_prive = 0;
                      
                        foreach ($data_prive as $prive) {
                              $total_prive += $prive->jumlah;

                        }
                        echo convert_rupiah_report($total_prive);?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td  class="codename" >Penambahan Modal</td>
                    <td  align="right">Rp</td>
                    <td></td>
                    <td align="right">
                   <?php 
                   $total_penambahan_modal = $total_laba_bersih - abs($total_prive);
                   echo convert_rupiah_report($total_penambahan_modal);?>
                    </td>
                </tr>
               <tr>
                    <td  class="codename footer-table" >Modal Akhir</td>
                    <td align="right" class="footer-table">Rp</td>
                    <td class="footer-table"></td>
                    <td align="right" class="footer-table">
                   <?php  
                   $total_modal_akhir = $total_modal + $total_penambahan_modal;
                   echo convert_rupiah_report($total_modal_akhir);?>
                    </td>
                </tr>
            </tbody>
        </table>
 
    </div>