<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

?>
<div class="col-lg-12">
    <?php
    if (! empty($message_success)) {
        echo '<div class="alert alert-success" role="alert">';
        echo $message_success;
        echo '</div>';
    }
    if (! empty($message)) {
        echo '<div class="alert alert-danger" role="alert">';
        echo $message;
        echo '</div>';
    }
    ?>

    <div class="modal fade" id="detail-pop-up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="exampleModalLabel"> Flazz Card Settlement </h3>
                </div>
                <div class="modal-body">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="create-new-debit-0" style="padding-top: 20px">
                            <div class="form-group col-sm-12">
                                <label class="col-sm-2 control-label">Total AR</label>

                                <div class="col-sm-10">
                                    <input placeholder="Total" value="" name="total" id="total" class="form-control" type="text" disabled>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-2 control-label">Biaya / Charge</label>

                                <div class="col-sm-10">
                                    <input placeholder="Charge" value="" name="charge" id="charge" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-2 control-label">Income</label>

                                <div class="col-sm-10">
                                    <input placeholder="Income" value="" name="income" id="income" class="form-control" type="text">
                                </div>
                            </div>                            
                            <div class="clearfix"></div>
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-submit">Submit</button>
                    <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <!-- /.panel-heading -->
        <div class="panel-body">
            <form id="formFilter" method="POST">
                <div class="clear-export"></div>
                    <table class="col-lg-8 form-table-cms">
                        <tbody>
                            <tr>
                                <td><label>Resto</label></td>
                                <td class="col-sm-8">
                                   <?php 
                                   echo form_dropdown('store_id', $all_store, $user_store['store_selected'],
                                  'id="store_id" field-name = "Resto" 
                                  class="form-control select2" autocomplete="on" '.$user_store['disabled_dropdown']);
                                     ?>                                    
                                </td>                                
                            </tr>                            
                            <tr>
                                <td><label>Bank</label></td>
                                <td>   <?php 
                                echo form_dropdown('bank_id', $all_bank, '', 
                                    'id="bank_id" field-name = "" 
                                    class="form-control select2" autocomplete="on"');
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Waktu Mulai</label></td>
                                <td class="col-sm-8">
                                        <div class='input-group date ' id='start_date'>
                                         <?php echo form_input(array('name' => 'start_date',
                                           'id' => 'input_start_date',
                                           'type' => 'text',
                                           'class' => 'form-control date',
                                           'onkeydown'=>'return false',
                                           'value' => date("Y-m-d")

                                           )); ?>
                                           <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div> 
                                </td>                          
                            </tr>
                            <tr>
                                <td><label>Waktu Akhir</label></td>
                                <td class="col-sm-8">
                                        <div class='input-group date ' id='end_date'>
                                         <?php echo form_input(array('name' => 'end_date',
                                           'id' => 'input_end_date',
                                           'type' => 'text',
                                           'class' => 'form-control date',
                                           'onkeydown'=>'return false',
                                           'value' => date("Y-m-d")

                                           )); ?>
                                           <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div> 
                                </td>  
                            </tr>
                            <tr>
                                <td colspan="4" align="right">
                                    <button id="filter_submit_flazz" class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
                                    <button id="export_xls" class="hide_btn btn btn-warning" style="display: none">Export XLS</button>
                                    <button id="export_pdf" class="hide_btn btn btn-success" style="display: none"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Export PDF</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </form>
            <table class="table table-striped table-bordered table-hover dt-responsive" id="dataTables-ar-flazz">
                <thead>
                <tr>
                    <th>Resto</th>
                    <th>Tanggal</th>
                    <th>No. Bill</th>
                    <th>Bank</th>
                    <th>No Kartu</th>
                    <th>Amount</th>
                    <th style="text-align: center; width:20%"><?php echo $this->lang->line('column_action'); ?></th>
                </tr>
                </thead>
            </table>
            <input type="hidden" id="dataProcessUrl"
                   value="<?php echo $data_url ?>"/>

            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->

<script data-main="<?php echo base_url('assets/js/main-account-receivable'); ?>"
        src="<?php echo base_url('assets/js/libs/require.js'); ?>"></script>